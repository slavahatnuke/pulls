import {IMapper, IPipeInput, IPipeOutput, isNodeOrFunction, pipe} from "./index";
import {nodes} from "./nodes";
import {filter} from "./filter";

type IWhenNoCase<Input extends any> = (x: Input) => Promise<void | string> | void | string;
type ICasesObject = { [P in string]: IPipeInput };


export function Cases<Input extends any, Output extends any>(
    casesObject: ICasesObject,
    caseMapper: IMapper<Input, keyof typeof casesObject>,
    whenNoCase: IWhenNoCase<Input> = () => undefined
) {
    const isValidCase = (casesObject: ICasesObject, caseName: string) => isNodeOrFunction(casesObject[caseName]);

    return (caseMapperOptional?: IMapper<Input, keyof typeof casesObject>, whenNoCaseOptional?: IWhenNoCase<Input>): IPipeOutput<Input, Output> => {
        const _caseMapper = caseMapperOptional || caseMapper;
        const _whenNoCase = whenNoCaseOptional || whenNoCase;

        const caseNodes = Object.keys(casesObject)
            .map((caseName) => {
                if (!isValidCase(casesObject, caseName)) {
                    throw new Error(`Invalid case: ${caseName}`)
                }

                return pipe(
                    filter<Input>(async (x) => caseName === (await _caseMapper(x))),
                    casesObject[caseName]
                )
            })


        return pipe<Input, Output>(
            filter<Input>(
                async (x) => {
                    const isValid = isValidCase(casesObject, await _caseMapper(x));

                    if (!isValid) {
                        await _whenNoCase(x)
                    }

                    return isValid
                }),
            nodes<Input, Output>(...caseNodes)
        )
    }
}
