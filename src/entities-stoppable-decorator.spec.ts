import {IStream, pipe, Pipe, pipeline} from "./index";
import {EntitiesStoppableDecorator} from "./entities-stoppable-decorator";
import {sequence} from "./sequence";
import {batch} from "./batch";
import {map} from "./map";
import {toArray} from "./toArray";
import {flat} from "./flat";

type ISomeUser = { id: number, name: string };
test('', async () => {
    const entities = Pipe( // entities emulation
        sequence(1e3),
        map((id) => ({id, name: `name ${id}`})),
        batch(3)
    );

    const onStop = jest.fn();

    const stoppableEntities = EntitiesStoppableDecorator(entities as IStream<ISomeUser[]>, {
        delay: 100,
        transform: pipe(
            flat(),
            (x: ISomeUser) => ({...x, transformed: `OK/${x.id}`})
        ),
        onStop
    });


    expect(await pipeline(
        stoppableEntities,
        async (x: ISomeUser) => {
            if (x.id > 3) {
                stoppableEntities.stop()
            }
            return x
        },
        toArray()
    )).toEqual([
        {
            "id": 0,
            "name": "name 0",
            "transformed": "OK/0"
        },
        {
            "id": 1,
            "name": "name 1",
            "transformed": "OK/1"
        },
        {
            "id": 2,
            "name": "name 2",
            "transformed": "OK/2"
        },
        {
            "id": 3,
            "name": "name 3",
            "transformed": "OK/3"
        },
        {
            "id": 4,
            "name": "name 4",
            "transformed": "OK/4"
        },
        {
            "id": 5,
            "name": "name 5",
            "transformed": "OK/5"
        }
    ])

    expect(onStop.mock.calls).toEqual([
        []
    ])
});
