import {Cases} from "./cases";
import {pipe, pipeline} from "./index";
import {streamsSync} from "./streamsSync";
import {toArray} from "./toArray";

type InputType1 = { type: string, id: number };

it('Cases', async function () {
    const cases = Cases<InputType1, string>({
        event: pipe((event) => `Event ${event.id}`),
        task: pipe((task) => `Task ${task.id}`),
    }, async (x) => x.type)

    expect(await pipeline(
        streamsSync(
            [{type: 'task', id: 1}, {type: 'task', id: 2}],
            [{type: 'event', id: 3}]
        ),
        cases(), // as function
        toArray()
    )).toEqual([
        "Task 1",
        "Event 3",
        "Task 2"
    ])
});

it('Cases / missed cases', async function () {

    const invalidValues: any[] = []

    const cases = Cases<InputType1, string>({
        event: pipe((event) => `Event ${event.id}`),
        task: pipe((task) => `Task ${task.id}`),
    }, async (x) => x.type)

    expect(await pipeline(
        streamsSync(
            [{type: 'task', id: 1}, {type: 'TaskAsInvalid', id: 2}],
            [{type: 'evenTypo', id: 3}]
        ),
        cases(
            undefined, (x) => {
                invalidValues.push(x)
            }
        ),
        toArray()
    )).toEqual([
        "Task 1"
    ])

    expect(invalidValues).toEqual([
        {
            "id": 3,
            "type": "evenTypo"
        },
        {
            "id": 2,
            "type": "TaskAsInvalid"
        }
    ])

});

it('Cases / missed cases / top level', async function () {

    const invalidValues: any[] = []

    const cases = Cases<InputType1, string>({
            event: pipe((event) => `Event ${event.id}`),
            task: pipe((task) => `Task ${task.id}`),
        },
        async (x) => x.type,
        (x) => {
            invalidValues.push(x)
        })

    expect(await pipeline(
        streamsSync(
            [{type: 'task', id: 1}, {type: 'TaskAsInvalid', id: 2}],
            [{type: 'evenTypo', id: 3}]
        ),
        cases(),
        toArray()
    )).toEqual([
        "Task 1"
    ])

    expect(invalidValues).toEqual([
        {
            "id": 3,
            "type": "evenTypo"
        },
        {
            "id": 2,
            "type": "TaskAsInvalid"
        }
    ])

});

it('Cases / invalid case value', async function () {
    const cases = Cases<InputType1, string>({
        // @ts-ignore
        event: 123123,
        task: pipe((task) => `Task ${task.id}`),
    }, (x) => 'event')

    expect(() => {
        cases(async (x) => x.type)
    }).toThrow('Invalid case: event')

});
