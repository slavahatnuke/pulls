import {IStream, IToFunctionInput, stream, toFunction} from "./index";
import {Defer} from "./lib/defer";

export type IInterval = IStream<number> & {
    stop: () => void
};

export function interval(ms: IToFunctionInput<number>, startImmediate: boolean = true): IInterval {
    let point = startImmediate ? null : Date.now();

    const toMs = toFunction<number>(ms)

    let delayDefer = Defer<boolean>();
    delayDefer.resolve(true)

    let delayTimer: any
    let stopped = false;

    const intervalStream = stream<number>(async () => {
        if (stopped) {
            return {done: true, value: 0}
        }

        const now = Date.now();

        if (!point) {
            point = now;
            return ({done: false, value: now})
        }

        const xDelay = now - point;

        if (xDelay < toMs()) {
            delayDefer = Defer<boolean>()
            const delayTimeout = toMs() - xDelay;
            delayTimer = setTimeout(() => delayDefer.resolve(true), delayTimeout)
            await delayDefer.promise

            if (stopped) {
                return {done: true, value: 0}
            }
        }

        point = Date.now();

        return ({done: false, value: point})
    }) as IInterval;

    intervalStream.stop = () => {
        if (delayTimer) {
            clearTimeout(delayTimer)
            delayTimer = undefined
        }
        stopped = true
        delayDefer.resolve(true)
    }

    return intervalStream
}
