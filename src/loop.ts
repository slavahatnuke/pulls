import {IStream, pull, Stream} from "./index";

export type ILoopCondition = (() => boolean) | (() => Promise<boolean>);

export function loop(condition: ILoopCondition): IStream<any> {
    return Stream<any>(() => async () => {
        return ({done: !await condition(), value: undefined as any})
    })
}

export async function loopCycle(condition: ILoopCondition) {
    return pull(loop(condition))
}
