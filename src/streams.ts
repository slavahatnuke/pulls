import {IMessage, IRead, IStream, Read, Stream, toStream} from "./index";
import {mem} from "./lib/mem";
import {Queue} from "./lib/queue";
import {isEOF} from "./lib/eof";
import {tickAsync} from "./lib/tick";

type IMessageType = 0 | 1;

const OK_TYPE = 0;
const ERROR_TYPE = 1;

export function streams<T extends any>(...rawStreams: any[]): IStream<T> {
    const streams = rawStreams.map((rawStream) => toStream<T>(rawStream));
    return Stream<T>(() => {
        const readers: IRead<T>[] = streams.map(Read)

        const outputs = Queue<[IMessageType, IMessage<T> | Error]>();

        const done = {
            done: true,
            value: undefined
        } as IMessage<T>;

        const handle = mem(async () => {
            if (!readers.length) {
                await outputs.write([
                    OK_TYPE,
                    done
                ])
                return;
            }

            await Promise.all(readers.map(async (reader) => {
                await tickAsync(async () => {
                    while (true) {
                        try {
                            const message = await reader();

                            if (message.done) {
                                return
                            }

                            await outputs.write([OK_TYPE, message])
                        } catch (error) {
                            await outputs.write([ERROR_TYPE, error])
                        }
                    }
                })
            }));

            await outputs.write([
                OK_TYPE,
                done
            ])

            await outputs.finish()
        });

        return async () => {
            handle()

            const outputMessage = await outputs.read();

            if (isEOF(outputMessage)) {
                return done;
            }

            const [type, payload] = outputMessage as [IMessageType, IMessage<T> | Error];

            if (type === ERROR_TYPE) {
                throw payload
            } else {
                return payload as IMessage<T>
            }
        }
    })
}
