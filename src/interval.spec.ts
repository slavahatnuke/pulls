import {pipeline} from "./index";
import {interval} from "./interval";
import {toArray} from "./toArray";
import {stopper} from "./stopper";

it('interval', async function () {

    const source = interval(100);
    let idx = 0;

    const d1 = Date.now();

    const sourceStopper = stopper();

    const out = await pipeline(
        source,
        sourceStopper,
        () => {
            idx++

            if (idx > 3) {
                sourceStopper.stop()
            }

            return idx;
        },
        toArray()
    );

    const d2 = Date.now();

    expect(out).toEqual([1, 2, 3, 4])
    expect(Math.round((d2 - d1) / 100)).toEqual(3)
});


it('interval / stop', async function () {

    const source = interval(100);
    let idx = 0;

    const d1 = Date.now();

    const out = await pipeline(
        source,
        () => {
            idx++

            if (idx > 3) {
                source.stop()
            }

            return idx;
        },
        toArray()
    );

    const d2 = Date.now();

    expect(out).toEqual([1, 2, 3, 4])
    expect(Math.round((d2 - d1) / 100)).toEqual(3)
});

it('interval / startImmediate false', async function () {
    const source = interval(100, false);
    let idx = 0;

    const d1 = Date.now();

    const s1 = stopper();
    const out = await pipeline(
        source,
        s1,
        () => {
            idx++

            if (idx > 3) {
                s1.stop()
            }

            return idx;
        },
        toArray()
    );

    const d2 = Date.now();

    expect(out).toEqual([1, 2, 3, 4])
    expect(Math.round((d2 - d1) / 100)).toEqual(4)
});
