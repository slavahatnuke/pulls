import {IMapper, INode, isFunction, isNode} from "./index";

export function map<Input extends any, Output extends any>(mapper: IMapper<Input, Output> | INode<Input, Output>): IMapper<Input, Output> | INode<Input, Output> {
    if (isFunction(mapper)) {
        return mapper as IMapper<Input, Output>;
    } else if (isNode(mapper)) {
        return mapper as INode<Input, Output>
    } else {
        throw new Error(`Expects function or node`)
    }
}

