import {pipeline} from "./index";
import {toArray} from "./toArray";
import {tap} from "./tap";

it('test reduceToArray', async function () {
    const expected: number[] = [];

    await pipeline(
        [
            1, // -> 100
            2,  // -> 200
            3 // -> 300
        ],
        (value) => value * 100,
        toArray<number>(), // [100, 200, 300]
        tap((value) => expected.push(value)) // [[100, 200, 300]]
    )

    expect(expected).toEqual([[100, 200, 300]])
});


it('good for testing', async function () {
    const result = await pipeline(
        [
            1, // -> 100
            2,  // -> 200
            4 // -> 400
        ],
        (value) => value * 100,
        toArray<number>(),
    );

    expect(result).toEqual([
        100,
        200,
        400
    ])
});
