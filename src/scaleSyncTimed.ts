import {IMapper, INode, IToFunctionInput, pipe} from "./index";
import {flat} from "./flat";
import {batchTimed} from "./batchTimed";

export function scaleSyncTimed<Input extends any, Output extends any>(quantity: IToFunctionInput<number>,
                                                                      timeout: IToFunctionInput<number>,
                                                                      mapper: IMapper<Input, Output>): INode<Input, Output> {
    return pipe<Input, Output>(
        batchTimed<Input>(quantity, timeout),
        async (values: Input[]) => Promise.all(
            values.map((value: Input) => mapper(value) as Output)),
        flat<Input[], Output>()
    ) as INode<Input, Output>
}
