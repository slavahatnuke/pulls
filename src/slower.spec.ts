import {pipeline} from "./index";
import {slower} from "./slower";
import {toArray} from "./toArray";
import {flat} from "./flat";

it('slower', async function () {

    const ms = 100;
    const d1 = Date.now()
    const result = await pipeline(
        [[], [], [], [], [1], [2], [3, 4, 5], []],
        slower((value) => !value.length, ms),
        flat(),
        toArray()
    );
    const d2 = Date.now()

    expect(Math.floor((d2 - d1) / ms)).toEqual(5)
    expect(result).toEqual([1, 2, 3, 4, 5])
});

it('slower.poke', async function () {

    const ms = 100;
    const d1 = Date.now()

    const s1 = slower<any[]>((value) => !value.length, ms);

    setTimeout(() => s1.poke(), 0)

    const result = await pipeline(
        [[]],
        s1,
        flat(),
        toArray()
    );

    const d2 = Date.now()

    expect(Math.floor((d2 - d1) / ms)).toEqual(0)
    expect(result).toEqual([])
});
