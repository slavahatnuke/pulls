import {IMessage, Read, Stream, toStream} from "./index";

export function streamsSync<T extends any>(...rawStreams: any[]) {
    const streams = rawStreams.map((rawStream) => toStream<T>(rawStream));
    return Stream<T>(() => {
        let readers = streams.map((stream) => Read<T>(stream));
        let idx = 0;

        const done = {
            done: true,
            value: undefined
        } as IMessage<T>;

        return async () => {
            while (true) {
                if (!readers.length) {
                    return done;
                }

                const reader = readers[idx];

                if (!reader) {
                    idx = 0
                    continue;
                }

                const message = await reader();

                if (message.done) {
                    // reader is finished delete it from readers
                    readers = readers.filter((aReader) => aReader !== reader)
                    continue;
                }

                idx++;

                return message;
            }

            return done
        }
    })
}
