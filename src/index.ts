// polyfill

if (!Symbol) {
    // @ts-ignore
    Symbol = (description: string) => description

    // @ts-ignore
    Symbol.for = (key) => key
}

if (!(Symbol as any).asyncIterator) {
    (Symbol as any).asyncIterator = Symbol.asyncIterator || Symbol("Symbol.asyncIterator");
}

if (!Symbol.iterator) {
    (Symbol as any).iterator = Symbol.iterator || Symbol("Symbol.iterator");
}

const NODE = Symbol(`Symbol.Pulls.NODE`);

// is
export function isFunction(x: any): boolean {
    return x instanceof Function || typeof x === 'function';
}

export function isObject(x: any): boolean {
    return x instanceof Object || (typeof x === 'object' && x !== null);
}

export function isSyncStream(x: any): boolean {
    return isObject(x) && !!x[Symbol.iterator]
}

export function isStream(x: any): boolean {
    return isObject(x) && !!x[Symbol.asyncIterator]
}

export function isNode(x: any): boolean {
    return isObject(x) && !!x[NODE]
}

export function isNodeOrFunction(x: any): boolean {
    return isNode(x) || isFunction(x);
}

export function isReadable(x: any): boolean {
    return isStream(x) || isSyncStream(x)
}

// helpers

function panic<T>(message: string): T {
    throw new Error(message)
}


export type IToFunctionInput<T extends any> = T | (IMapAnyToType<T>);

export function toFunction<T extends any>(x: IToFunctionInput<T>): IMapAnyToType<T> {
    if (isFunction(x)) {
        return x as IMapAnyToType<T>
    } else {
        return () => x as T
    }
}

export type IMap<Input extends any, Output extends any> = (x: Input) => Output;
export type IAsyncMap<Input extends any, Output extends any> = (x: Input) => Promise<Output>;
export type IMapper<Input extends any, Output extends any> = IMap<Input, Output> | IAsyncMap<Input, Output>;
export type IMapAnyToType<T extends any> = (...args: any[]) => T;

export function MustBe(isType: IMap<any, boolean>, message: string = 'Invalid.'): any {
    if (!isFunction(isType)) {
        return panic(`Expects function`)
    }

    return function mustBe<T extends any>(value: any) {
        if (!isType(value)) {
            return panic<T>(`${message} GOT ${typeof value} = ${value}`)
        }
    }
}

function syncPipe<MapperType extends IMap<any, any>, Input extends any, Output extends any>(...mappers: MapperType[]) {
    return function (input: Input): Output {
        return mappers.reduce((current, mapper) => mapper(current), input) as Output | any
    };
}

function asyncPipe<MapperType extends IMap<any, any>, Input extends any, Output extends any>(...mappers: MapperType[]) {
    return async function (input: Input): Promise<Output> {
        return await mappers.reduce(async (current, mapper) => mapper(await current), (async () => input)()) as Output | any
    };
}

const mustBeFunction = MustBe(isFunction, `Expects function.`);
const mustBeNode = MustBe(isNode, `Expects node.`);
const mustBeNodeOrFunction = MustBe(isNodeOrFunction, `Expects node or function.`);
const mustBeReadable = MustBe(isReadable, `Expects readable.`);

// --- logic

// Streams
export type ICursor<T extends any> = {
    next: IRead<T>
}

export type IStream<T extends any> = {
    [Symbol.asyncIterator](): ICursor<T>
}

export type IMessage<T extends any> = { done: boolean; value: T };

export type IRead<T extends any> = (() => Promise<IMessage<T>>) | (() => IMessage<T>);
export type IReadFactory<T extends any> = () => IRead<T>

export type IReadFactoryOrStream<T extends any> = IReadFactory<T> | any;

export function Stream<T extends any>(readFactoryOrStream: IReadFactoryOrStream<T>): IStream<T> {
    if (isReadable(readFactoryOrStream)) {
        return toStream<T>(readFactoryOrStream)
    }

    return {
        [Symbol.asyncIterator]: () => ({next: readFactoryOrStream() as IRead<T>})
    };
}

export type IReadOrStream<T extends any> = IRead<T> | any;

export function stream<T extends any>(readOrStream: IReadOrStream<T>): IStream<T> {
    if (isReadable(readOrStream)) {
        return toStream<T>(readOrStream)
    }

    return Stream<T>(() => readOrStream)
}

function syncStreamToStream<T extends any>(syncIterable: any): IStream<T> {
    return Stream<T>((): IRead<T> => {
        const iterator = syncIterable[Symbol.iterator]();
        return () => iterator.next() as IMessage<T>
    }) as IStream<T>;
}

export function toStream<T extends any>(x: any): IStream<T> {
    if (isStream(x)) {
        return x as IStream<T>
    } else if (isSyncStream(x)) {
        return syncStreamToStream<T>(x)
    } else {
        return mustBeReadable(x)
    }
}

// Read && Pull
export function Read<T extends any>(stream: IStream<T>): IRead<T> {
    const instance = toStream(stream)[Symbol.asyncIterator]();
    return () => instance.next() as IMessage<T>
}

export async function pull<T extends any>(stream: IStream<any>, defaultOutput: any = undefined as any): Promise<T | undefined> {
    let last = defaultOutput;
    for await (const value of stream) {
        last = value
    }
    return last;
}

// just alias
export const run = pull;

// Pipe
export function Pipe<Input extends any, Output extends any>(stream: any, ...nodes: IPipeInput[]): IStream<Output> {
    const streamMapper = nodeToStreamMapper(toNode(
        pipe<Input, Output>(...nodes as IPipeInput[])
    ));

    return streamMapper(toStream<Input>(stream)) as IStream<Output>;
}

export async function pipeline<Input extends any, Output extends any>(input: any, ...nodes: IPipeInput[]): Promise<Output> {
    return await pull(Pipe<Input, any>(input, ...nodes)) as Output;
}

// Pipe
export type IPipeInput = INode<any, any> | IMapper<any, any>;

export type IPipeOutput<Input extends any, Output extends any> = INode<Input, Output> | IMapper<Input, Output>;


function optimizePipeInputsToPureAsyncPipe(inputs: IPipeInput[]) {
    return inputs
        .reduce((accumulator, input) => {
            const last = accumulator[accumulator.length - 1];
            if (last && isFunction(last[0]) && isFunction(input)) {
                last.push(input)
            } else {
                accumulator.push([input])
            }
            return accumulator;
        }, [] as IPipeInput[][])
        .map((items) => items.length > 1 ? asyncPipe(...(items as IMap<any, any>[])) : items[0]);
}

export function pipe<Input extends any, Output extends any>(...inputs: IPipeInput[]): IPipeOutput<Input, Output> {
    if (inputs.length === 1 && isNode(inputs[0])) {
        return inputs[0] as INode<Input, Output>
    }

    const nodes = optimizePipeInputsToPureAsyncPipe(inputs);

    if (nodes.every(isFunction)) {
        if (nodes.length === 1) {
            return nodes[0]
        } else {
            return asyncPipe<any, Input, Output>(...nodes) as IMapper<any, Output>
        }
    }

    const mappers: IStreamMapper<any, any>[] = nodes
        .map(toNode)
        .map(nodeToStreamMapper);

    return Node<Input, Output>(
        syncPipe<IMap<any, any>, IStream<Input>, IStream<Output>>(...mappers)
    )
}

export function toNode<Input extends any, Output extends any>(x: any): INode<Input, Output> {
    if (isNode(x)) {
        return x;
    } else if (isFunction(x)) {
        return _map(x as IMap<any, any>);
    } else {
        return mustBeNodeOrFunction(x)
    }
}

// Node
export type IStreamMapper<Input extends any, Output extends any> = (input: IStream<Input>) => IStream<Output>;
export type INode<Input extends any, Output extends any> = { [NODE]: IStreamMapper<Input, Output> };

export function Node<Input extends any, Output extends any>(mapper: IStreamMapper<Input, Output>): INode<Input, Output> {
    return {
        [NODE]: mapper
    }
}

export type INodeCursorMapper<Input extends any, Output extends any> = (read: IRead<Input>) => IRead<Output>;

export function node<Input extends any, Output extends any>(cursorMapper: INodeCursorMapper<Input, Output>): INode<Input, Output> {
    return Node<Input, Output>((input) => {
        const read: IRead<Input> = Read<Input>(input);
        return stream<Output>(cursorMapper(read))
    })
}

export function nodeToStreamMapper<Input extends any, Output extends any>(node: INode<Input, Output>): IStreamMapper<Input, Output> {
    mustBeNode(node)
    return node[NODE] as IStreamMapper<Input, Output>;
}

export function _map<Input extends any, Output extends any>(mapper: IMapper<Input, Output> | INode<Input, Output>): INode<Input, Output> {
    if (isNode(mapper)) {
        return mapper as INode<Input, Output>
    } else {
        mustBeFunction(mapper)
        return node<Input, Output>((read): IRead<Output> => async () => {
            const message = await read();
            const {done, value} = message;

            if (done) {
                return message
            }

            const output = await (mapper as IMap<Input, Output>)(value as Input);
            return {done, value: output} as IMessage<Output | any>
        });
    }
}
