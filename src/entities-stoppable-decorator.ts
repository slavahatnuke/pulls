import {IPipeInput, IStream, IToFunctionInput, Pipe, toFunction} from "./index";
import {stopper} from "./stopper";
import {slower} from "./slower";
import {IStoppable} from "./stoppable";

export type IEntitiesStoppableDecorator<T extends any> = IStream<T> & IStoppable<T> & {
    poke(): Promise<void>
};

type IEntitiesStoppableDecoratorOptions<T extends any> = {
    onStop?: () => Promise<void>,
    detectEmpty?: (messages: T[]) => Promise<boolean> | boolean,
    delay?: IToFunctionInput<number>,
    transform?: IPipeInput
};

export function EntitiesStoppableDecorator<T extends any>(entitiesStream: IStream<T[]>, {
    onStop = (): any => undefined,
    delay = toFunction<number>(0),
    transform = ((x: any) => x) as IPipeInput,
    detectEmpty = (messages: T[]) => (Array.isArray(messages) && !messages.length)
}: IEntitiesStoppableDecoratorOptions<T> = {}) {
    delay = toFunction(delay);

    const aStopper = stopper(onStop);
    const aSlower = slower<T[]>(detectEmpty, delay);

    const hasDelay = !!delay();

    const nodes: (IPipeInput | undefined)[] = [
        aStopper,
        hasDelay ? aSlower : undefined,
        transform
    ].filter(Boolean)

    const stream = Pipe(
        entitiesStream,
        ...nodes as IPipeInput[]
    ) as IEntitiesStoppableDecorator<T>;

    let stopped = false;

    stream.stop = async () => {
        stopped = true
        const promise = aStopper.stop();
        await stream.poke()
        return promise
    }

    stream.isStopped = () => {
        return stopped;
    }

    stream.poke = async () => {
        if (hasDelay) {
            aSlower.poke()
        }
    }

    return stream
}
