import {pipeline} from "./index";
import {tap} from "./tap";

it('tap', async function () {

    const expected: any = [];
    const expected2: any = [];
    const collect = (value: any) => expected.push(value);
    const collect2 = (value: any) => expected2.push(value);

    await pipeline(
        [1, 2, 3],
        tap(collect),
        tap(collect2),
        // tap(console.log),
    )

    expect(expected).toEqual([
        1,
        2,
        3
    ])

    expect(expected2).toEqual([
        1,
        2,
        3
    ])
});
