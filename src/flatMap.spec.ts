import {pipe, pipeline, toStream} from "./index";
import {flatMap} from "./flatMap";
import {map} from "./map";
import {tap} from "./tap";

it('test flatMap default', async function () {
    const expected: number[] = [];

    await pipeline(
        [
            [1], // -> 1
            2,  // -> 2
            [33], // -> 33
            [[44]] // -> [44] only one array should be unwrapped
        ],
        flatMap(),
        tap((value) => expected.push(value))
    )

    expect(expected).toEqual([
        1,
        2,
        33,
        [
            44
        ]
    ])
});


it('test flatMap functional', async function () {
    const expected: number[] = [];

    await pipeline(
        [
            1, // -> 10
            2,  // -> 20
            3 // -> 30
        ],
        flatMap<number, number>((value) => value * 10),
        tap((value) => expected.push(value))
    )

    expect(expected).toEqual([
        10,
        20,
        30
    ])
});


it('test flatMap with pipe', async function () {
    const expected: number[] = [];

    await pipeline(
        [
            1, // -> 10
            2,  // -> 20
            3 // -> 30
        ],
        flatMap<number, number>(pipe(
            map((value) => value * 10)
        )),
        tap((value) => expected.push(value))
    )

    expect(expected).toEqual([
        10,
        20,
        30
    ])
});

it('test flatMap stream', async function () {
    const expected: number[] = [];

    await pipeline(
        [
            1, // -> 10
            2,  // -> 20
            3 // -> 30
        ],
        flatMap<number, number>(async (value) => toStream<number>([value, value * 2, value * 3])),
        tap((value) => expected.push(value))
    )

    expect(expected).toEqual([
        1,
        2,
        3,

        2,
        4,
        6,

        3,
        6,
        9
    ])
});
