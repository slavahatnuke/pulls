export type IDefer<T extends any> = {
    promise: Promise<T>,

    reject: (error?: any) => void,
    resolve: (value: T) => void,

    rejected: boolean,
    resolved: boolean,
}

export function Defer<T extends any>(): IDefer<T> {
    // @ts-ignore
    const defer = {
        promise: undefined,

        reject: undefined,
        resolve: undefined,

        rejected: false,
        resolved: false,
    } as IDefer<T>;

    defer.promise = new Promise<T>((innerResolve, innerReject) => {
        defer.resolve = innerResolve;
        defer.reject = innerReject;
    });

    return defer;
}
