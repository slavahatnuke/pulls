
export type IamEOF = {}
export const EOF: IamEOF = {}

export function isEOF(value: any): boolean {
    return value === EOF
}
