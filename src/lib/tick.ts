import {isFunction, isObject} from "../index";
import {Defer} from "./defer";

type ITickRunner = () => any;
type ITickAsyncRunner = () => Promise<any> | any;

export type ITick = (fn: ITickRunner) => void

export function Tick(): ITick {
    if (isObject(process) && isFunction(process.nextTick)) {
        return (fn: ITickRunner) => {
            process.nextTick(fn)
        }
    } else if (isFunction(setImmediate)) {
        return (fn: ITickRunner) => {
            setImmediate(fn)
        }
    } else {
        return (fn: ITickRunner) => {
            setTimeout(fn, 0)
        }
    }
}

export const tick = Tick();

export async function tickAsync(fn: ITickAsyncRunner) {
    const defer = Defer<boolean>();

    tick(async () => {
        try {
            await fn()
            defer.resolve(true)
        } catch {
            defer.resolve(true)
        }
    })

    await defer.promise;
}
