import {Defer} from "./defer";

it('defer', async function () {

    const defer = Defer<string>();

    setTimeout(() => defer.resolve('OK123'), 0)
    const result = await defer.promise;

    expect(result).toEqual('OK123')
});


it('defer / reject', async function () {

    const defer = Defer<string>();

    setTimeout(() => defer.reject(new Error('err12345')), 0)
    await expect(async () => await defer.promise).rejects.toThrow('err12345')
});
