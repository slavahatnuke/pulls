import {IToFunctionInput, toFunction} from "../index";
import {IQueue, Queue, QUEUE_EVENT_READ} from "./queue";
import {Defer} from "./defer";

export function BufferQueue<T extends any>(size: IToFunctionInput<number>): IQueue<T> {
    const desired = toFunction<number>(size)
    const queue = Queue<T>();

    async function write(value: T) {
        if (queue.length() < desired()) {
            queue.write(value)
        } else {
            const defer = Defer<boolean>();

            const unsubscribe = queue.subscribe((event) => {
                switch (event.type) {
                    case QUEUE_EVENT_READ:
                        if (queue.length() < desired()) {
                            queue.write(value)
                            defer.resolve(true)
                            unsubscribe()
                        }

                        break;
                }
            });

            await defer.promise;
        }
    }

    return {
        write,
        read: queue.read,
        length: queue.length,
        subscribe: queue.subscribe,
        finish: queue.finish,
        destroy: queue.destroy,
    }
}
