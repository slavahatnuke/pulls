import {Topic} from "./topic";
import {Defer, IDefer} from "./defer";
import {EOF, IamEOF} from "./eof";

export type IQueueEvent<T extends any> = {
    type: string,
    payload?: T
}

export type IQueueListener<T extends any> = (event: IQueueEvent<T>) => Promise<void> | void;

export type IQueue<T extends any> = {
    write: (value: T) => Promise<void>,
    read: () => Promise<T | IamEOF>
    subscribe: (listener: IQueueListener<any>) => () => void
    length: () => number
    finish: () => Promise<void>
    destroy: () => void
}

export const QUEUE_EVENT_WRITE = 'write';
export const QUEUE_EVENT_READ = 'read';
export const QUEUE_EVENT_FINISH = 'finish';
export const QUEUE_EVENT_FINISHED = 'finished';
export const QUEUE_EVENT_DESTROY = 'destroy';
export const QUEUE_EVENT_DESTROYED = 'destroyed';

export function Queue<T extends any>(): IQueue<T> {

    const topic = Topic<IQueueEvent<any>>();
    const {publish, subscribe} = topic;

    let messages: { value: T, defer: IDefer<boolean> }[] = [];
    let finishing = false;
    let finished = false;
    let finishDefer: undefined | IDefer<boolean> = undefined;

    async function read(): Promise<T | IamEOF> {
        const message = messages.shift();

        if (message) {
            message.defer.resolve(true)

            publish({
                type: QUEUE_EVENT_READ,
                payload: message
            })

            return message.value;
        } else {
            if (finishing) {
                _doFinish();
                return EOF;
            }

            const readDefer = Defer<T | IamEOF>();

            const unsubscribe = subscribe((event) => {
                switch (event.type) {
                    case QUEUE_EVENT_WRITE:
                        const message = messages.shift()

                        if (message) {
                            message.defer.resolve(true)

                            publish({
                                type: QUEUE_EVENT_READ,
                                payload: message
                            })

                            readDefer.resolve(message.value)
                            unsubscribe()
                        }
                        break;

                    case QUEUE_EVENT_DESTROY:
                    case QUEUE_EVENT_FINISH:
                        readDefer.resolve(EOF)
                        unsubscribe()
                        _doFinish();
                        break;
                }
            });

            return readDefer.promise;
        }
    }

    async function write(value: T): Promise<void> {
        if (finishing) {
            throw new Error(`Finishing, impossible to write`)
        }

        const defer = Defer<boolean>();
        const message = {value, defer};
        messages.push(message);

        publish({
            type: QUEUE_EVENT_WRITE,
            payload: message
        })

        await defer.promise
    }


    async function finish() {
        if (finishDefer) {
            await finishDefer.promise;
            return;
        }

        finishDefer = finishDefer || Defer<boolean>();
        finishing = true;

        if (!messages.length) {
            finishDefer.resolve(true)
        } else {
            const unsubscribe = subscribe((event) => {
                switch (event.type) {
                    case QUEUE_EVENT_FINISHED:
                        finishDefer!.resolve(true)
                        unsubscribe();
                        break;
                    case QUEUE_EVENT_DESTROY:
                        finishDefer!.reject(new Error(`Queue: queue is destroyed`))
                        unsubscribe();
                        break;
                }
            });
        }

        publish({
            type: QUEUE_EVENT_FINISH
        })

        await finishDefer.promise
    }

    function destroy() {
        publish({
            type: QUEUE_EVENT_DESTROY
        })

        messages.forEach((message) => message.defer.reject(new Error(`Queue: message is destroyed`)))
        messages = [];

        finishing = true;
        finished = true;

        publish({
            type: QUEUE_EVENT_DESTROYED
        })

        topic.destroy()
    }

    function _doFinish() {
        if (!finished) {
            finished = true
            publish({
                type: QUEUE_EVENT_FINISHED
            })
        }
    }

    function length() {
        return messages.length
    }


    return {
        read,
        write,
        subscribe,
        length,
        finish,
        destroy,
    }
}
