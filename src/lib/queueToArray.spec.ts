import {Queue} from "./queue";
import {queueToArray} from "./queueToArray";

it('queueToArray', async function () {

    const queue = Queue<number>();

    queue.write(1)
    queue.write(2)
    queue.write(3)

    expect(await queueToArray(queue)).toEqual([1,2,3])
});
