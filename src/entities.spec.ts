import {entities} from "./entities";
import {isObject, pipeline} from "./index";
import {toArray} from "./toArray";

it('entities', async function () {
    const users = [
        {id: 1},
        {id: 2},
        {id: 3},
    ];

    const Users = entities<object>(
        async () => users.shift()
    );

    expect(await pipeline(
        Users,
        toArray()
    )).toEqual([
        {
            "id": 1
        },
        {
            "id": 2
        },
        {
            "id": 3
        }
    ])
});


it('entities with checker', async function () {
    const users = [
        {id: 1},
        {id: 2},
        {id: 3},
    ];

    const Users = entities<object>(
        async () => users.shift(),
        async (x) => await isObject(x)
    );

    expect(await pipeline(
        Users,
        toArray()
    )).toEqual([
        {
            "id": 1
        },
        {
            "id": 2
        },
        {
            "id": 3
        }
    ])
});
