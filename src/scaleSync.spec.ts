import {pipeline} from "./index";
import {sequence} from "./sequence";
import {scale} from "./scale";
import {delay} from "./lib/delay";
import {toArray} from "./toArray";
import {scaleSync} from "./scaleSync";

it('scaleSync', async function () {


    const d1 = Date.now()
    const out = await pipeline(
        sequence(4),
        scaleSync<number, number>(1, async (value) => {
            await delay(100)
            return value
        }),
        toArray()
    ) as any[];

    const d2 = Date.now()

    expect(out.sort()).toEqual([0, 1, 2, 3])
    expect(Math.round((d2 - d1) / 100)).toEqual(4)
});


it('scaleSync 2', async function () {

    const d1 = Date.now()
    const out = await pipeline(
        sequence(4),
        scaleSync<number, number>(2, async (value) => {
            await delay(100)
            return value
        }),
        toArray()
    ) as any[];

    const d2 = Date.now()

    expect(out.sort()).toEqual([0, 1, 2, 3])
    expect(Math.round((d2 - d1) / 100)).toEqual(2)
});

it('scaleSync 4', async function () {


    const d1 = Date.now()
    let idx = 0

    const out = await pipeline(
        sequence(4),
        scaleSync<number, number>(4, async (value) => {
            await delay(100)
            idx++
            return value
        }),
        toArray()
    ) as any[];

    const d2 = Date.now()

    expect(out.sort()).toEqual([0, 1, 2, 3])
    expect(idx).toEqual(4)
    expect(Math.round((d2 - d1) / 100)).toEqual(1)
});

it('scaleSync 3', async function () {
    const d1 = Date.now()
    let idx = 0

    const out = await pipeline(
        sequence(4),
        scaleSync<number, number>(3, async (value) => {
            await delay(100)
            idx++
            return value
        }),
        toArray()
    ) as any[];

    const d2 = Date.now()

    expect(out.sort()).toEqual([0, 1, 2, 3])
    expect(idx).toEqual(4)
    expect(Math.round((d2 - d1) / 100)).toEqual(2)
});


it('scaleSync 3 in dynamic', async function () {
    const d1 = Date.now()
    let idx = 0

    const out = await pipeline(
        sequence(4),
        scaleSync<number, number>(3, async (value) => {
            idx++
            await delay(100 * idx); // 100, 200, 300, 400

            return value
        }),
        toArray()
    ) as any[];

    const d2 = Date.now()

    expect(out.sort()).toEqual([0, 1, 2, 3])
    expect(idx).toEqual(4)
    expect(Math.round((d2 - d1) / 100)).toEqual(7)
});

it('scaleSync / error', async function () {
    await expect(async () => {
        await pipeline(
            sequence(4),
            scaleSync<number, number>(4, async (value) => {
                await delay(100)
                throw new Error(`Woooppepeeee`)
            }),
            toArray()
        )
    }).rejects.toThrow(`Woooppepeeee`)
});
