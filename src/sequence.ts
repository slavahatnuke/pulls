import {IStream, Stream} from "./index";

export function sequence(size = 0): IStream<number> {
    return Stream<number>(() => {
        let value = -1;
        return () => {
            value++;
            return {done: value >= size, value}
        }
    })
}
