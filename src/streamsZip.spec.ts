import {Pipe, pipeline} from "./index";
import {sequence} from "./sequence";
import {toArray} from "./toArray";
import {StreamsZip, streamsZip} from "./streamsZip";

it('streamsZip', async function () {

    const out = await pipeline(
        streamsZip(
            sequence(3), // 0, 1, 2
            sequence(3), // 0, 1, 2
            sequence(3), // 0, 1, 2
        ),
        toArray()
    );

    expect(out).toEqual([
        [0, 0, 0],
        [1, 1, 1],
        [2, 2, 2]
    ])
});


it('streamsZip with Pipe', async function () {

    const out = await pipeline(
        streamsZip(
            Pipe(sequence(3), (value) => value * 10), // 0, 10 , 20
            Pipe(sequence(3), (value) => value * 100), // 0, 100 , 200
            Pipe(sequence(3), (value) => value * 1000), // 0, 1000 , 2000
        ),
        toArray()
    );

    expect(out).toEqual([
        [0, 0, 0],
        [10, 100, 1000],
        [20, 200, 2000]
    ])
});


it('streamsSync / diff size and default undefined value as EOF', async function () {

    const out = await pipeline(
        streamsZip(
            sequence(3), // 0, 1, 2
            sequence(2), // 0, 1
            sequence(1), // 0
            sequence(0), // nothing
        ),
        toArray()
    );

    expect(out).toEqual([
        [0, 0, 0, undefined],
        [1, 1, undefined, undefined],
        [2, undefined, undefined, undefined],
    ])
});


it('StreamsZip / custom NULL as EOF', async function () {
    const customStreamZip = StreamsZip<number, null>(() => null);

    const out = await pipeline(
        customStreamZip(
            sequence(3), // 0, 1, 2
            sequence(2), // 0, 1
            sequence(1), // 0
        ),
        toArray()
    );

    expect(out).toEqual([
        [0, 0, 0],
        [1, 1, null],
        [2, null, null]
    ])
});
