import {IMessage, INode, IRead, isReadable, IStream, node, Read, Stream} from "./index";

export function flatStream<Input extends any, Output extends any>(read: IRead<Input | IStream<Input>>): IStream<Output> {
    return Stream<Output>(() => {
        let readChild: IRead<Output> | null = null;
        // TODO @@@slava optimize code and make it readable
        return async () => {
            while (true) {
                if (readChild) {
                    const child = await readChild();

                    const {done: childDone} = child;

                    if (childDone) {
                        readChild = null
                        // continue to read input
                    } else {
                        return child as IMessage<Output>;
                    }
                }

                const input = await read() as IMessage<Output | IStream<Output>>
                const {done, value} = input

                if (done) {
                    return input as IMessage<Output>;
                }

                if (isReadable(value)) {
                    readChild = Read<Output>(value as IStream<Output>)
                    const child = await readChild();

                    const {done: childDone} = child;

                    if (childDone) {
                        // TODO @@@slava add test case if child is empty by default
                        // TODO @@@slava heh could be used as filter too by streams
                        // TODO @@@slava add empty() & value(xx): IStream
                        readChild = null
                        // continue to read input
                        // organized by while
                    } else {
                        return child as IMessage<Output>;
                    }
                } else {
                    return input as IMessage<Output>
                }
            }
        }
    });
}

export function flat<Input extends any, Output extends any>(): INode<Input, Output> {
    return node<Input | IStream<Input>, Output>((read): IRead<Output> =>
        Read(flatStream(read)))
}

