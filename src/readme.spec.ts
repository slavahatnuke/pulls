import {pipeline, stream} from "./index";
import {map} from "./map";
import {filter} from "./filter";
import {reduce} from "./reduce";
import {sequence} from "./sequence";
import {tap} from "./tap";

it('Basic example', async function () {
    // consume array as stream
    const expected = await pipeline(
        stream([1, 2, 3]), // automatically converts array to stream as async interable

        map((x) => x * 2), // multiple; = 2, 4, 6

        filter((x) => x > 2), // filter only x > 2; = 4, 6

        // types input:number, output:number[]
        reduce<number, number[]>((acc, value) => [...acc, value], []), // collect to array as expected

        // tap(console.log) // prints output
    );

    // expected: [4, 6]
    expect(expected).toEqual([4, 6])
});


it('Basic example 2', async function () {
    // consume async iterator as stream and sum some values of 1m
    const expected = await pipeline(
        sequence(1000000), // creates low CPU/memory stream

        map((x) => x * 2), // multiple; = 2, 4, 6...

        filter((x) => x > 2), // filter only x > 2; = 4, 6....

        // types input:number, output:number
        reduce<number, number>((acc, value) => acc + value, 0), // sum values

        // tap(console.log) // prints output; 999998999998
    );

    // expected: 999998999998
    expect(expected).toEqual(999998999998)
});
