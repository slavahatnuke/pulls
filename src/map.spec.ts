import {pipe, pipeline} from "./index";
import {map} from "./map";
import {tap} from "./tap";


it('map', async function () {
    const expected: number[] = [];

    await pipeline(
        [5, 10, 15],
        map<number, number>((x) => x + 100),
        tap<number>((value) => expected.push(value))
    )

    expect(expected).toEqual([
        105,
        110,
        115
    ])
});


it('map as function', async function () {
    const expected: number[] = [];

    await pipeline(
        [5, 10, 15],
        (x) => x + 100,
        tap<number>((value) => expected.push(value))
    )

    expect(expected).toEqual([
        105,
        110,
        115
    ])
});

it('test node as mapper for map', async function () {
    async function* Gen() {
        yield 1
        yield 2
        yield Promise.resolve(3)
        yield Promise.resolve(34)
    }

    const expected: number[] = [];

    await pipeline(
        [5, 10, 15],
        map<number, number>(pipe(
            map((x) => x + 100)
        )),
        tap<number>((value) => expected.push(value))
    )

    expect(expected).toEqual([
        105,
        110,
        115
    ])
});
