import {pipeline} from "./index";
import {sequence} from "./sequence";
import {tap} from "./tap";

it('test sequence', async function () {
    const expected: any = [];
    const collect = (value: any) => expected.push(value);

    await pipeline(
        sequence(5),
        tap(collect)
    )
    expect(expected).toEqual([
        0,
        1,
        2,
        3,
        4
    ])
});
