import {NodeReadable, NodeTransform, NodeWritable} from "./node-stream";
import {pipeline} from "./index";

import {toArray} from "./toArray";
import {Readable, Transform} from "stream";
import {delay} from "./lib/delay";
import {tap} from "./tap";

it('NodeReadable', async function () {
    async function* generate() {
        yield 'hello';
        yield 'streams';
    }

    expect(
        await pipeline(
            NodeReadable<string>(Readable.from(generate())),
            (value) => `${value}/OK`,
            toArray()
        )
    ).toEqual([
        "hello/OK",
        "streams/OK"
    ])
});


it('NodeTransform', async function () {
    function OkayNodeTransformer() {
        return new Transform({
            objectMode: true,
            transform(chunk: any, encoding: string, callback: any) {
                callback(null, `${chunk}/OKAY_TRANSFORM`)
            }
        });
    }

    expect(
        await pipeline(
            [1, 22, 34],
            NodeTransform(OkayNodeTransformer()),
            (value) => `${value}/OK_MAP`,
            toArray()
        )
    ).toEqual([
        "1/OKAY_TRANSFORM/OK_MAP",
        "22/OKAY_TRANSFORM/OK_MAP",
        "34/OKAY_TRANSFORM/OK_MAP"
    ])
});


it('NodeTransform / error', async function () {
    function ErrorNodeTransformer() {
        return new Transform({
            objectMode: true,
            transform(chunk: any, encoding: string, callback: any) {
                callback(new Error(`wooooppppeeee error`))
            }
        });
    }

    await expect(async () => {
        await pipeline(
            [1, 22, 34],
            NodeTransform(ErrorNodeTransformer()),
            (value) => `${value}/SHOULD_NOT_BE_MAPPED`,
            toArray()
        )
    }).rejects.toThrow(`wooooppppeeee error`)
});

it('NodeWritable', async function () {
    const {Writable} = require('stream');

    let timeMarker = Date.now();
    const timeLog: any[] = [];
    const writes: any[] = [];
    const delayInMs = 300;

    function OkayDelayedWriter(delayInMs: number) {
        return new Writable({
            objectMode: true,
            async write(chunk: any, encoding: string, callback: any) {
                await delay(delayInMs);
                writes.push(chunk)
                callback()
            }
        });
    }

    expect(
        await pipeline(
            [10, 11, 12],
            NodeWritable(OkayDelayedWriter(delayInMs)),
            tap((value) => {
                const now = Date.now();
                timeLog.push({diff: now - timeMarker, value})
                timeMarker = now;
            }),
            toArray()
        )
    ).toEqual([
        10, 11, 12
    ])

    const normalizedTimeLog = timeLog.map(({diff, value}) => {
        return {diff: Math.floor((diff + 1) / delayInMs), value}
    });
    expect(normalizedTimeLog).toEqual([
        {
            "diff": 1,
            "value": 10
        },
        {
            "diff": 1,
            "value": 11
        },
        {
            "diff": 1,
            "value": 12
        }
    ])

    expect(writes).toEqual([
        10,
        11,
        12
    ])
});


it('NodeWritable / error', async function () {
    const {Writable} = require('stream');
    await expect(async () => {
        await pipeline(
            [10, 11, 12],
            NodeWritable(new Writable({
                objectMode: true,
                write(chunk: any, encoding: string, callback: (error?: (Error | null)) => void) {
                    throw new Error(`woops error!!`)
                }
            })),
        )
    }).rejects.toThrow(`woops error!!`)

});


it('NodeWritable / error 2', async function () {
    const {Writable} = require('stream');
    await expect(async () => {
        await pipeline(
            [10, 11, 12],
            NodeWritable(new Writable({
                objectMode: true,
                write(chunk: any, encoding: string, callback: any) {
                    callback(new Error(`woops error 2 xxxxxxx`))
                }
            })),
        )
    }).rejects.toThrow(`woops error 2 xxxxxxx`)

});
