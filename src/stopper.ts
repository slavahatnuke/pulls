import {IMessage, INode, IRead, node} from "./index";
import {IStoppableInterface} from "./stoppable";
import {mem} from "./lib/mem";


export type IStopper<T extends any> =
    INode<T, T> &
    IStoppableInterface;

export function stopper<T extends any>(onStop: () => Promise<void> | void = async () => undefined): IStopper<T> {
    let stopped = false;

    const stoppableNode = node<T, T>((read: IRead<T>) => {
        return async () => {
            if (stopped) {
                return {
                    done: true,
                    value: undefined
                } as IMessage<T>
            } else {
                const message = await read() as IMessage<T>;

                if (stopped) {
                    return {
                        done: true,
                        value: undefined
                    } as IMessage<T>
                } else {
                    return message
                }
            }
        }
    }) as IStopper<T>;

    stoppableNode.stop = mem(async () => {
        stopped = true
        await onStop()
    })

    stoppableNode.isStopped = () => {
        return stopped
    }

    return stoppableNode
}
