import {IMessage, isFunction, isObject, IStream, stream} from "./index";

export function isCursor(x: any): boolean {
    return isObject(x) && isFunction(x.next)
}

export function cursorToStream<T extends any>(x: any): IStream<T> {
    if (!isCursor(x)) {
        throw new Error(`Expects cursor`)
    }
    return stream<T>(() => x.next() as IMessage<T>) as IStream<T>;
}
