import {IMapper, INode, pipe} from "./index";
import {catchError} from "./catchError";

export class ValueError<T extends any> extends Error {

    constructor(public value: T, message: string) {
        super(message);
    }
}

export function valueError<T extends any>(node: INode<T, T> | IMapper<T, T>): INode<T, T> | IMapper<T, T> {
    let current: T;

    return pipe(
        (value) => {
            current = value
            return value
        },
        node,
        catchError((error) => {
            const valueError = new ValueError<T>(current, error.message);

            if (error instanceof Error && error.stack) {
                valueError.stack = error.stack
            }

            throw valueError
        })
    )
}
