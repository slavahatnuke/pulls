import {IMessage, INode, IPipeInput, Node, Pipe, pipeline, Read, Stream, toNode} from "./index";
import {mem} from "./lib/mem";
import {writeable} from "./writeable";
import {catchError} from "./catchError";
import {tickAsync} from "./lib/tick";
import {streams} from "./streams";

const OK = 0;
const ERROR = 1;

type IMessageType = 0 | 1
type INodesMessage<T> = [IMessageType, T | Error]

export function nodes<Input extends any, Output extends any>(...nodes: IPipeInput[]): INode<Input, Output> {
    const _nodes = nodes.map(toNode);

    return Node<Input, Output>((input) => {
        return Stream<Output>(() => {

            const nodeStreams = Array(_nodes.length)
                .fill(0)
                .map(() => writeable<Input>());

            const errorStream = writeable<Error>()

            const mappedNodeStreams = nodeStreams.map((nodeStream, idx) => {
                return Pipe(nodeStream, nodes[idx])
            });

            const dataStream = streams<Input>(...mappedNodeStreams);

            const outputStream = streams<INodesMessage<Input>>(
                Pipe(dataStream, (value) => [OK, value]),
                Pipe(errorStream, (error) => [ERROR, error]),
            )

            const readOutput = Read<INodesMessage<Input>>(outputStream)

            const handle = mem(async () => {
                await tickAsync(async () => {
                    try {
                        await pipeline(
                            input,
                            async (value) => {
                                await Promise.all(nodeStreams.map(async (stream) => {
                                    await stream.write(value as Input)
                                }))
                            },
                            catchError(async (error) => {
                                await errorStream.write(error)
                            })
                        )

                        await Promise.all(
                            [
                                ...nodeStreams.map(async (stream) => {
                                    await stream.finish()
                                }),
                                errorStream.finish()
                            ]
                        )
                    } catch (error) {
                        await errorStream.write(error)
                    }
                });

            });

            return async () => {
                handle();

                const message = await readOutput();

                if (message.done) {
                    return message as IMessage<Output>
                }

                const [type, value] = message.value as INodesMessage<Input>

                if (type === ERROR) {
                    throw value
                }

                return {
                    done: false,
                    value
                } as IMessage<Output>
            }
        })
    })
}
