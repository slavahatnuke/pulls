import {pipeline} from "./index";
import {sequence} from "./sequence";
import {stopper} from "./stopper";
import {toArray} from "./toArray";
import {filter} from "./filter";
import {delay} from "./lib/delay";
import {tap} from "./tap";

it('stopper', async function () {

    let stopped = false
    let stoppedCounter = 0
    const onStop = async () => {
        await delay(10)
        stopped = true
        stoppedCounter++;
    };

    const sourceStopper = stopper<number>(onStop);

    expect(sourceStopper.isStopped()).toEqual(false);

    const out = await pipeline(
        sequence(1e3),
        sourceStopper,
        async (value) => {
            if (value > 5) {
                await sourceStopper.stop()
                await sourceStopper.stop() // test dup calls; onStop should be called once
                await sourceStopper.stop() // test dup calls; onStop should be called once
                return null
            }
            return value
        },
        filter((value) => value !== null),
        toArray()
    );
    expect(sourceStopper.isStopped()).toEqual(true);
    expect(out).toEqual([0, 1, 2, 3, 4, 5])
    expect(stopped).toEqual(true)
    expect(stoppedCounter).toEqual(1)
});


it('stopper / filter out delayed', async function () {

    const onStop = async () => {
    };

    const sourceStopper = stopper<number>(onStop);

    expect(sourceStopper.isStopped()).toEqual(false);

    setTimeout(sourceStopper.stop, 50)

    const out = await pipeline(
        [1],

        tap(async () => {
            await delay(100)
        }),

        sourceStopper,

        toArray()
    );

    expect(out).toEqual([])
});
