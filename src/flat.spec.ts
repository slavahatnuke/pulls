import {pipeline} from "./index";
import {flat} from "./flat";
import {tap} from "./tap";

it('flat', async function () {
    const expected: number[] = [];

    await pipeline(
        [
            [1], // -> 1
            2,  // -> 2
            [],  // -> nothing
            [3], // -> 3
            [[4]] // -> [4] only one array should be unwrapped
        ],
        flat(),
        tap((value) => expected.push(value))
    )

    expect(expected).toEqual([
        1,
        2,
        3,
        [
            4
        ]
    ])
});
