import {IMessage, INode, IRead, isFunction, isReadable, IStream, node, Node, toStream} from "./index";

export function NodeReadable<T extends any>(readable: any): IStream<T> {
    if (!isReadable(readable)) {
        throw new Error(`Expects readable`)
    }

    return toStream(readable);
}

export function NodeTransform<Input extends any, Output extends any>(transform: any): INode<Input, Output> {
    if (!isReadable(transform)) {
        throw new Error(`Expects readable`)
    }

    if (!isFunction(transform.pipe)) {
        throw new Error(`Expects pipe method`)
    }

    const {Readable} = require('stream');

    // TODO @@@slava error handling
    // TODO @@@slava types
    return Node<Input, Output>((input: IStream<Input>) => {
        const readable = Readable.from(toStream(input) as AsyncIterable<Input>);
        return toStream(Readable.from(readable.pipe(transform))) as IStream<Output>
    })
}

export function NodeWritable<Input extends any, Output extends any>(writable: any): INode<Input, Output> {
    if (!writable) {
        throw new Error(`Expects stream`)
    }

    if (!isFunction(writable.write)) {
        throw new Error(`Expects write method`)
    }

    return node<Input, Output>((read: IRead<Input>) => {
        writable.on('error', () => null);

        return async () => {
            const message = await read() as IMessage<Input>;
            const {done, value} = message;

            if (done) {
                await new Promise<any>((resolve, reject) => {
                    try {
                        writable.end((error: Error) => {
                            if (error) {
                                reject(error)
                            } else {
                                resolve(true)
                            }
                        })
                    } catch (error) {
                        reject(error)
                    }
                })

                return message
            }

            await new Promise<any>((resolve, reject) => {
                try {
                    writable.write(value, (error: Error) => {
                        if (error) {
                            reject(error)
                        } else {
                            resolve(true)
                        }
                    })
                } catch (error) {
                    reject(error)
                }
            })

            return message as IMessage<Output | any>
        }
    })
}
