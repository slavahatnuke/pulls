import {pipeline} from "./index";
import {toArray} from "./toArray";
import {ValueError} from "./valueError";
import {delay} from "./lib/delay";
import {catchError} from "./catchError";
import {tap} from "./tap";

it('catchError', async function () {
    const errors: any = [];


    expect(await pipeline(
        [
            1, 2, 22
        ],
        (v) => {
            throw new Error(`Woopsss ${v}`)
        },
        catchError(async (error) => {
            errors.push(error.message)
        }),
        toArray()
    )).toEqual([])

    expect(errors).toEqual([
        "Woopsss 1",
        "Woopsss 2",
        "Woopsss 22"
    ])
});


it('catchError re-throw', async function () {
    const values: any = [];
    const errors: any = [];

    await expect(async () => {
        await pipeline(
            [
                1, 2, 22
            ],
            (v) => {
                throw new Error(`Woopsss ${v}`)
            },
            catchError(async (error) => {
                errors.push(error.message)
                throw error
            }),
            tap((value) => values.push(value))
        )
    }).rejects.toThrow(`Woopsss 1`)

    expect(errors).toEqual([
        "Woopsss 1"
    ])
});


it('catchError and continue', async function () {
    const errors: any = [];

    const output = await pipeline(
        [
            1, 2, 22
        ],
        (v) => {
            throw new ValueError(v, `Wooppppsss!! ${v}`)
        },
        catchError<ValueError<number>, number, number>(async (error, next) => {
            const {message, value} = error
            errors.push([message, value])
            await delay(10)
            await next(error.value + 1000)
        }),
        toArray()
    );

    expect(errors).toEqual([
        [
            "Wooppppsss!! 1",
            1
        ],
        [
            "Wooppppsss!! 2",
            2
        ],
        [
            "Wooppppsss!! 22",
            22
        ]
    ])
    expect(output).toEqual([
        1001,
        1002,
        1022
    ])
});
