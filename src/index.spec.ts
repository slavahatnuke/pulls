import {pipe, Pipe, pipeline, pull, run, stream, Stream, toStream} from "./index";
import {toArray} from "./toArray";
import {delay} from "./lib/delay";
import {map} from "./map";
import {tap} from "./tap";

it('pipeline', async function () {

    const expected: any = [];
    const collect = (value: any) => expected.push(value);

    await pipeline(
        [1, 2, 3],
        tap(collect),
        // tap(console.log),
    )

    expect(expected).toEqual([
        1,
        2,
        3
    ])
});

it('pipe', async function () {

    const expected: any = [];
    const collect = (value: any) => expected.push(value);

    await pipeline(
        [1, 2, 3, 4, 5, 10],
        pipe(
            pipe(
                tap(collect)
            )
        ),
        // tap(console.log),
    )

    expect(expected).toEqual([
        1,
        2,
        3,
        4,
        5,
        10,
    ])
});


it('map as function', async function () {

    const expected: any = [];
    const collect = (value: any) => expected.push(value);

    await pipeline(
        [1, 2, 3, 4, 5, 10],
        pipe(
            (x) => x * 10,
            pipe(
                tap(collect)
            )
        ),
        // tap(console.log),
    )

    expect(expected).toEqual([
        10,
        20,
        30,
        40,
        50,
        100
    ])
});


it('pipe optimize inputs to function', async function () {

    const expected: any = [];
    const expected2: any = [];
    const collect = (value: any) => expected.push(value);

    const functionalAsyncPipe = pipe(async (value) => {
        await delay(10)
        collect(value)
        return value;
    });

    expect(functionalAsyncPipe instanceof Function).toEqual(true)

    await pipeline(
        [1, 2, 3],
        functionalAsyncPipe,
        tap((value) => {
            expected2.push(value)
        }),
        // tap(console.log)
    )

    expect(expected).toEqual([
        1,
        2,
        3
    ])
    expect(expected2).toEqual([
        1,
        2,
        3
    ])
});

it('for await iterations for 123 array', async function () {

    const items: number[] = [1, 2, 3];
    const result: number[] = [];

    for await (const value of items) {
        // nothing todo just read
        result.push(value)

        await delay(20)
    }

    expect(result).toEqual([
        1,
        2,
        3
    ])
});

it('test iterable / low level', async function () {
    const numbers1 = [11, 22, 33];
    expect(numbers1[Symbol.iterator]().next()).toEqual({"done": false, "value": 11})
    expect(numbers1[Symbol.iterator]().next()).toEqual({"done": false, "value": 11})

    expect(await toStream(numbers1)[Symbol.asyncIterator]().next()).toEqual({"done": false, "value": 11})
    expect(await toStream(numbers1)[Symbol.asyncIterator]().next()).toEqual({"done": false, "value": 11})
});

it('toStream / test iterable', async function () {
    const numbers1 = [11, 22, 33];

    const expected: number[] = [];
    for await (const x of toStream<number>(numbers1)) {
        expected.push(x)
    }

    expect(expected).toEqual([
        11,
        22,
        33
    ])
});

it('Stream / test iterable', async function () {
    const numbers1 = [11, 22, 33];

    const expected: number[] = [];
    for await (const x of Stream<number>(numbers1)) {
        expected.push(x)
    }

    expect(expected).toEqual([
        11,
        22,
        33
    ])
});

it('stream / test iterable', async function () {
    const numbers1 = [11, 22, 33];

    const expected: number[] = [];
    for await (const x of stream<number>(numbers1)) {
        expected.push(x)
    }

    expect(expected).toEqual([
        11,
        22,
        33
    ])
});

it('async generator support', async function () {
    async function* Gen() {
        yield 1
        yield 2
        yield Promise.resolve(3)
        yield Promise.resolve(34)
    }

    const expected: number[] = [];

    await pipeline(
        Gen(),
        tap<number>((value) => expected.push(value))
    )

    expect(expected).toEqual([
        1,
        2,
        3,
        34
    ])
});

it('sync generator support', async function () {
    function* GenSync() {
        yield 1
        yield 2
        yield 7
    }

    const expected: number[] = [];

    await pipeline(
        GenSync(),
        tap<number>((value) => expected.push(value))
    )

    expect(expected).toEqual([
        1,
        2,
        7
    ])
});

it('pull / returns last value', async function () {
    expect(await pull(toStream([]), 'defaultValueHere')).toEqual('defaultValueHere')
    expect(await pull(toStream([3, 4, 5]), 'defaultValueHere')).toEqual(5)
});

it('run / returns last value', async function () {
    expect(await run(toStream([]), 'defaultValueHere')).toEqual('defaultValueHere')
    expect(await run(toStream([3, 4, 5]), 'defaultValueHere')).toEqual(5)
});

it('pipeline / returns last value', async function () {

    expect(await pipeline([3, 4, 5])).toEqual(5)
    expect(
        await pipeline(
            [3, 4, 5],
            (x) => x * 10,
            map((x) => x + 1)
        )
    ).toEqual(51)
});

it('Pipe is a stream mapper', async function () {

    const inputStream = toStream<number>([3, 4, 5]);
    const outputStream = Pipe<number, number>(
        inputStream,
        (x) => x * 10,
        pipe(
            (x) => x + 1,
            map((x) => x + 4),
        )
    );

    expect(await pull(inputStream)).toEqual(5)
    expect(await pull(outputStream)).toEqual(55)
});

it('pipeline / throws error if invalid readable', async function () {

    await expect(async () => {
        await pipeline(null, () => {
            throw new Error(`wooops`)
        })
    }).rejects.toThrow(`Expects readable. GOT object = null`)

    await expect(async () => {
        await pipeline([1], () => {
            throw new Error(`wooops`)
        })
    }).rejects.toThrow(`wooops`)
});

it('tap functional', async function () {
    const expected1: number[] = [];
    const expected2: number[] = [];

    await pipeline(
        [20, 21, 25],
        tap((value) => {
            expected1.push(value)
            return value + 1000 // should not be applied
        }),
        tap((value) => expected2.push(value))
    )

    expect(expected1).toEqual([
        20, 21, 25
    ])
    expect(expected2).toEqual([
        20, 21, 25
    ])
});

it('optimized pipe input', async function () {
    expect(await pipeline(
        [20, 21, 25],
        (v) => v + 1,
        async (v) => {
            await delay(10)
            return v + 2
        },
        (v) => v + 3,
        toArray()
    )).toEqual([
        26, 27, 31
    ])

    expect(await pipeline(
        [20, 21, 25],
        map((v) => v + 1),
        map((v) => v + 2),
        map(async (v) => {
            await delay(20)
            return v + 3
        }),
        toArray()
    )).toEqual([
        26, 27, 31
    ])
});
