import {IMessage, IStream, stream} from "./index";
import {Queue} from "./lib/queue";
import {isEOF} from "./lib/eof";

export type IWriteable<T extends any> = IStream<T> &
    {
        write: (value: T) => Promise<void>,
        finish: () => Promise<void>,
        destroy: () => void
    };

export function writeable<T extends any>(): IWriteable<T> {
    const queue = Queue<T>();

    const writeableStream = stream<T>(async () => {
        const value = await queue.read();

        if (isEOF(value)) {
            await queue.finish();

            return {
                done: true,
                value: undefined
            } as IMessage<T>
        } else {
            return {
                done: false,
                value
            } as IMessage<T>
        }
    }) as IWriteable<T>;

    writeableStream.write = async (value: T) => {
        await queue.write(value)
    }

    writeableStream.finish = async () => {
        await queue.finish()
    }

    writeableStream.destroy = async () => {
        await queue.destroy()
    }

    return writeableStream
}
