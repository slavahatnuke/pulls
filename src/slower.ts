import {IMapper, INode, IToFunctionInput, toFunction} from "./index";
import {tap} from "./tap";
import {delay as libDelay} from "./lib/delay";
import {Defer} from "./lib/defer";

type ISlowerOutput<T extends any> = IMapper<T, T> | INode<T, T>;
export type ISlower<T extends any> = ISlowerOutput<T> & {
    poke: () => void
};

export function slower<T extends any>(detect: (value: T) => boolean | Promise<boolean>, ms: IToFunctionInput<number>): ISlower<T> {
    const toDelay = toFunction<number>(ms);

    let defer = Defer<boolean>();

    const node = tap<T>(async (value) => {
        if (await detect(value)) {
            defer.resolve(true);
            defer = Defer<boolean>();

            (async () => {
                await libDelay(toDelay())
                defer.resolve(true)
            })().catch(() => defer.resolve(true))

            await defer.promise
        }
    }) as ISlower<T>;

    node.poke = () => defer.resolve(true)

    return node;
}
