import {writeable} from "./writeable";
import {pipeline} from "./index";
import {toArray} from "./toArray";
import {tick} from "./lib/tick";

it('writeable', async function () {

    const numbers = writeable<number>();


    tick(async () => {
        await numbers.write(1)
        await numbers.write(2)
        await numbers.write(3)
        await numbers.finish()
    })

    const out = await pipeline(
        numbers,
        toArray()
    );

    expect(out).toEqual([1, 2, 3])
});
