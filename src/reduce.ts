import {IMessage, INode, IRead, isFunction, node} from "./index";

export type IReduceCallback<Input extends any, Output extends any> = (accumulator: Output, currentValue: Input) => Output | Promise<Output>;

export function reduce<Input extends any, Output extends any>(reducer: IReduceCallback<Input, Output>, initialValue: Output): INode<Input, Output> {
    if (!isFunction(reducer)) {
        throw new Error(`Expects function`)
    }
    return node<Input, Output>((read): IRead<Output> => {
        let working = true

        return async () => {
            if (!working) {
                return {done: true, value: undefined as any} as IMessage<Output>;
            }

            let accumulator = initialValue as Output;

            while (working) {
                const result = await read();

                const {done, value} = result;

                if (done) {
                    working = false
                } else {
                    accumulator = await reducer(accumulator, value);
                }
            }

            return {done: false, value: accumulator} as IMessage<Output>;
        }
    });
}
