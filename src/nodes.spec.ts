import {pipe, pipeline, stream} from "./index";
import {nodes} from "./nodes";
import {toArray} from "./toArray";
import {filter} from "./filter";
import {map} from "./map";

it('nodes', async function () {

    const out = await pipeline(
        [1],
        nodes<number, number>(
            (value) => value + 1,
            (value) => value + 10,
        ),
        toArray()
    );

    expect(out).toEqual([2, 11])
});


it('nodes for different pipes', async function () {

    const out = await pipeline(
        [
            {
                type: 'A',
                payload: 100
            },
            {
                type: 'B',
                payload: 10
            }
        ],
        nodes<number, number>(
            pipe(
                filter(({type}) => type === 'A'),
                map(({payload}) => `A:${payload}`)
            ),
            pipe(
                filter(({type}) => type === 'B'),
                map(({payload}) => `B:${payload}`)
            )
        ),
        toArray()
    );

    expect(out).toEqual([
        "A:100",
        "B:10"
    ])
});


it('nodes / errors', async function () {


    await expect(async () => {
        await pipeline(
            [1],
            nodes<number, number>(
                (value) => value + 1,
                (value) => {
                    throw new Error(`Wooopppppiii`)
                },
            ),
            toArray()
        );
    }).rejects.toThrow('Wooopppppiii')
});


it('nodes / errors on source', async function () {


    await expect(async () => {
        await pipeline(
            stream(() => {
                throw new Error(`Wzzzzzz`)
            }),
            nodes<number, number>(
                (value) => value + 1,
            ),
            toArray()
        );
    }).rejects.toThrow('Wzzzzzz')
});
