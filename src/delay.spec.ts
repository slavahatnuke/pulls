import {pipeline} from "./index";
import {sequence} from "./sequence";
import {delay} from "./delay";

it('delay', async function () {
    const ms = 100;

    const d1 = Date.now()

    await pipeline(
        sequence(3),
        delay(ms)
    )

    const d2 = Date.now()

    expect(Math.round((d2 - d1) / ms)).toEqual(3)
});


it('delay / dynamic', async function () {
    const ms = 100;

    const d1 = Date.now()

    let idx = 0
    await pipeline(
        sequence(3),
        delay(() => {
            idx++
            return ms * idx; // 100, 200, 300 = 600ms in total
        })
    )

    const d2 = Date.now()

    expect(Math.round((d2 - d1) / ms)).toEqual(6)
});
