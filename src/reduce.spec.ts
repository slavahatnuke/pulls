import {pipeline} from "./index";
import {reduce} from "./reduce";
import {tap} from "./tap";

it('test reduce', async function () {
    const expected: number[] = [];

    await pipeline(
        [
            10, // -> 9
            20,  // -> 19
            30 // -> 29
        ],
        (value) => value - 1,
        reduce<number, number[]>(async (a, v) => [...a, v], []), // [9, 19, 29]
        tap((value) => expected.push(value)) // [[9, 19, 29]]
    )

    expect(expected).toEqual([[9, 19, 29]])
});
