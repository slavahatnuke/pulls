import {pipeline, stream} from "./index";
import {sequence} from "./sequence";
import {toArray} from "./toArray";
import {filter} from "./filter";
import {delay} from "./lib/delay";
import {stoppable} from "./stoppable";

it('stoppable', async function () {
    let stopped = false
    let stoppedCounter = 0

    const onStop = async () => {
        await delay(10)
        stopped = true
        stoppedCounter++;
    };

    const source = stoppable<number>(sequence(1e3), onStop);
    expect(source.isStopped()).toEqual(false);

    const out = await pipeline(
        source,
        async (value) => {
            if (value > 5) {
                await source.stop()
                await source.stop() // test dup calls; onStop should be called once
                await source.stop() // test dup calls; onStop should be called once
                return null
            }
            return value
        },
        filter((value) => value !== null),
        toArray()
    );

    expect(source.isStopped()).toEqual(true);
    expect(out).toEqual([0, 1, 2, 3, 4, 5])
    expect(stopped).toEqual(true)
    expect(stoppedCounter).toEqual(1)
});


it('stoppable / filter out delayed', async function () {
    const source = stoppable<number>(stream<number>(async () => {
        await delay(100)
        return {done: false, value: 100500}
    }));

    setTimeout(source.stop, 50)

    const out = await pipeline(
        source,
        toArray()
    );

    expect(out).toEqual([])
});
