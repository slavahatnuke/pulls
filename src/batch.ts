import {IMessage, INode, IRead, IToFunctionInput, node, toFunction} from "./index";

export function batch<T extends any>(size: IToFunctionInput<number> = 1): INode<T, T[]> {
    const toSize = toFunction<number>(size);
    return node<T, T[]>((read): IRead<T[]> => {
        let batched: T[] = [];
        let finished = false;
        return async function () {
            if (finished) {
                return {done: true, value: undefined as any} as IMessage<any>
            }

            while (true) {
                const message: IMessage<T> = await read();
                const {done, value} = message;

                if (done) {
                    finished = true
                    if (batched.length) {
                        const output = {done: false, value: batched};
                        batched = []
                        return output
                    } else {
                        return message;
                    }
                }

                batched.push(value);
                if (batched.length >= toSize()) {
                    const output = {done: false, value: batched};
                    batched = []
                    return output
                }
            }
        }
    })
}
