import {IMessage, INode, IToFunctionInput, node, toFunction} from "./index";
import {mem} from "./lib/mem";
import {tick, tickAsync} from "./lib/tick";
import {BufferQueue} from "./lib/buffer";
import {queueToArray} from "./lib/queueToArray";
import {Queue} from "./lib/queue";
import {isEOF} from "./lib/eof";


const OK = 0;
const ERROR = 1;

type IMessageType = 0 | 1;

type IamOutputMessage<T extends any> = [IMessageType, T | Error];

export function batchTimed<T extends any>(size: IToFunctionInput<number>, timeout: IToFunctionInput<number> = 0): INode<T, T[]> {
    const toSize = toFunction<number>(size);
    const toTimeout = toFunction<number>(timeout);

    if (toSize() <= 0) {
        throw new Error(`Size is required`)
    }

    return node<T, T[]>((read) => {
        let queue = BufferQueue<IMessage<T>>(toSize);
        let output = Queue<IamOutputMessage<T[]>>();

        const done = {
            done: true,
            value: [undefined]
        } as IMessage<T[]>;

        let interval: any = undefined;
        let finishing = false;

        function setupInterval() {
            if (!!toTimeout() && !finishing) {
                interval = setTimeout(async () => {
                    try {
                        await triggerFlush()
                        setupInterval();
                    } catch (error) {
                        await output.write([ERROR, error])
                        await output.finish();
                    }
                }, toTimeout())
            }
        }

        const handle = mem(() => {
            tick(async () => {
                setupInterval();

                while (true) {
                    try {
                        const message = await read();

                        if (message.done) {
                            await triggerFinish()
                            break;
                        }

                        const writePromise = queue.write(message);

                        if (toSize() && queue.length() >= toSize()) {
                            await triggerFlush()
                        }

                        await writePromise
                    } catch (error) {
                        await output.write([ERROR, error]);
                    }
                }
            })

        });

        async function triggerFlush() {
            await tickAsync(async () => {
                const prevQueue = queue;
                queue = BufferQueue<IMessage<T>>(toSize);

                const messages = await queueToArray<IMessage<T>>(prevQueue);

                if (messages.length) {
                    const values = messages.map(({value}) => value);

                    await output.write([OK, values])
                }
            })
        }

        async function triggerFinish() {
            finishing = true;

            if (interval) {
                clearInterval(interval)
            }

            await triggerFlush()
            await output.finish()
        }

        return async () => {
            handle()

            const internalMessage = await output.read() as IamOutputMessage<T>;

            if (isEOF(internalMessage)) {
                return done
            }

            const [type, payload]: IamOutputMessage<T> = internalMessage as IamOutputMessage<T>;

            if (type === ERROR) {
                throw payload as Error
            } else {
                return {
                    done: false,
                    value: payload as T[]
                } as IMessage<T[]>
            }
        }
    })
}
