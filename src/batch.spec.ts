import {pipeline} from "./index";
import {toArray} from "./toArray";
import {batch} from "./batch";
import {tap} from "./tap";
import {catchError} from "./catchError";

it('batch', async function () {

    expect(await pipeline(
        [1, 2, 3, 4, 5],
        batch(2),
        toArray()
    )).toEqual([
        [
            1,
            2
        ],
        [
            3,
            4
        ],
        [
            5
        ]
    ])

    expect(await pipeline(
        [1, 2, 3, 4],
        batch(2),
        toArray()
    )).toEqual([
        [
            1,
            2
        ],
        [
            3,
            4
        ]
    ])


    expect(await pipeline(
        [1, 2, 3],
        batch(3),
        toArray()
    )).toEqual([
        [
            1,
            2,
            3
        ]
    ])


    expect(await pipeline(
        [],
        batch(3),
        toArray()
    )).toEqual([])
});


it('batch / size as function', async function () {

    expect(await pipeline(
        [1, 2, 3, 4, 5],
        batch(() => 3),
        toArray()
    )).toEqual([
        [1, 2, 3],
        [4, 5]
    ])
});

it('batch / source error', async function () {

    await expect(async () => {
        await pipeline(
            [1, 2],
            tap(async (id) => {
                throw new Error(`Woooohhh ${id}`)
            }),
            batch(2),
            toArray()
        )
    }).rejects.toThrow(`Woooohhh 1`)
});


it('batch / source errors catched', async function () {

    expect(await pipeline(
        [1, 2, 3],
        tap(async (id) => {
            throw new Error(`Woooohhh ${id}`)
        }),
        batch(2),
        catchError((error, resolveAs) => resolveAs(error.message)), // snoozed by catchError
        toArray()
    )).toEqual(["Woooohhh 1", "Woooohhh 2", "Woooohhh 3"])
});
