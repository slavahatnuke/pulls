import {IMessage, IRead, IToFunctionInput, Read, Stream, toFunction, toStream} from "./index";

export function StreamsZip<ValueType extends any, EOFType extends any>(eofFactory: IToFunctionInput<EOFType>) {
    const EOF = toFunction<EOFType>(eofFactory)();

    return function streamsZip(...rawStreams: any[]) {
        const streams = rawStreams.map((rawStream) => toStream<ValueType>(rawStream));

        return Stream<(ValueType | EOFType)[]>(() => {
            let readers: (IRead<ValueType> | undefined)[] = streams.map((stream) => Read<ValueType>(stream));
            let activeReaders = readers.length;

            const done = {
                done: true,
                value: undefined as ValueType
            } as IMessage<(ValueType | EOFType)[]>;


            return async () => {
                while (true) {
                    if (activeReaders <= 0) {
                        return done;
                    }

                    const messages = await Promise.all(
                        readers.map(async (reader, idx) => {
                            if (reader) {
                                const message = await reader();

                                if (message.done) {
                                    readers[idx] = undefined;
                                    activeReaders--;
                                }

                                return message;
                            } else {
                                return done;
                            }
                        }));

                    if (messages.every((message) => message.done)) {
                        return done;
                    }

                    const values = messages.map((message) => message.done ? EOF : message.value);

                    return {
                        value: values,
                        done: false
                    } as IMessage<(ValueType | EOFType)[]>
                }

                return done
            }
        })
    }
}

export function streamsZip<T>(...rawStreams: any[]) {
    const zip = StreamsZip<T, undefined>(() => undefined);
    return zip(...rawStreams)
}
