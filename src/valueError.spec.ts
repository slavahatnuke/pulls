import {valueError, ValueError} from "./valueError";
import {pipe, pipeline} from "./index";
import {catchError} from "./catchError";
import {toArray} from "./toArray";

it('ValueError', function () {
    const valueError = new ValueError<number>(125, 'its error');
    expect(valueError instanceof Error).toEqual(true)
    expect(valueError.value).toEqual(125)
    expect(valueError.message).toEqual('its error')
});

it('valueError could be resolved', async function () {

    const out = await pipeline(
        [1, 2, 3],
        valueError(
            pipe(
                (value) => {
                    throw Error(`Woopssssssszzz: ${value}`)
                }
            )
        ),
        catchError((error, resolve) => {
            resolve(error)
        }),
        toArray()
    ) as ValueError<number>[];

    const mappedErrors = out.map(({message, value}) => ({
        message,
        value
    }));

    expect(mappedErrors).toEqual([
        {
            "message": "Woopssssssszzz: 1",
            "value": 1
        },
        {
            "message": "Woopssssssszzz: 2",
            "value": 2
        },
        {
            "message": "Woopssssssszzz: 3",
            "value": 3
        }
    ])
});

it('valueError to throw', async function () {

    try {

        await pipeline(
            [1, 2, 3],
            valueError(
                pipe(
                    (value) => {
                        throw Error(`Woopssssssszzz: ${value}`)
                    }
                )
            )
        );

        throw new Error(`invalid error`)
    } catch (error) {
        // console.error(error)
        expect(error.message).toEqual(`Woopssssssszzz: 1`)
        expect(error.value).toEqual(1)
    }


});
