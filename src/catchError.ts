import {IMessage, INode, node} from "./index";

export type ICatchErrorHandler<ErrorType extends Error, ValueType extends any> = (error: ErrorType, next: (value: ValueType) => any) => any ;

export function catchError<ErrorType extends Error, Input extends any, Output extends any>(errorHandler: ICatchErrorHandler<ErrorType, Output>): INode<Input, Output> {
    return node<Input, Output>((read) => async () => {
        while (true) {
            try {
                return await read() as IMessage<Output | any>;
            } catch (error) {
                let called = false
                let value = undefined;

                await errorHandler(error, (anyValue: Output) => {
                    called = true
                    value = anyValue;
                });

                if (called) {
                    return {done: false, value} as IMessage<Output | any>;
                }
            }
        }
    })
}
