import {IMapper, INode, IPipeOutput, isFunction, isNode, IStream, pipe} from "./index";
import {flat} from "./flat";
import {map} from "./map";


type IFlatMapMapper<Input extends any, Output extends any> =
    IMapper<Input, Output | IStream<Output>>
    | INode<Input, Output | IStream<Output>>;

export function flatMap<Input extends any, Output extends any>(mapper?: IFlatMapMapper<Input, Output>): IPipeOutput<Input, Output> {
    if (isFunction(mapper) || isNode(mapper)) {
        return pipe<Input, Output>(
            map<Input, Output | IStream<Output>>(mapper as IMapper<Input, Output | IStream<Output>>),
            flat<Input, Output>()
        )
    } else {
        return flat<Input, Output>()
    }
}
