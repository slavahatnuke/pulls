import {loopCycle, loop} from "./loop";
import {pull} from "./index";
import {delay} from "./lib/delay";

it('loopCycle', async function () {
    let idx = 0;

    await loopCycle(async () => {
        await delay(10)
        idx++;
        return idx < 3;
    })
    expect(idx).toEqual(3)
});

it('loop', async function () {
    let idx = 0;

    const loopStream = loop(async () => {
        await delay(10)
        idx++;
        return idx < 5;
    });

    await pull(loopStream)
    expect(idx).toEqual(5)
});
