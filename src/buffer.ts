import {IMessage, IToFunctionInput, node, toFunction} from "./index";
import {mem} from "./lib/mem";
import {tickAsync} from "./lib/tick";
import {BufferQueue} from "./lib/buffer";
import {isEOF} from "./lib/eof";

const OK = 0;
const ERROR = 1;

type IMessageType = 0 | 1
type IBufferMessage<T> = [IMessageType, T | Error]

export function buffer<T extends any>(size: IToFunctionInput<number>) {
    const _size = toFunction<number>(size);

    return node<T, T>((read) => {

        const bufferQueue = BufferQueue<IBufferMessage<IMessage<T>>>(_size);

        const handle = mem(() => {
            tickAsync(async () => {
                try {
                    while (true) {
                        try {
                            const message = await read();
                            await bufferQueue.write([OK, message])

                            if (message.done) {
                                break
                            }

                        } catch (error) {
                            await bufferQueue.write([ERROR, error])
                        }
                    }

                    await bufferQueue.finish()

                } catch (error) {
                    await bufferQueue.write([ERROR, error])
                    await bufferQueue.finish()
                }
            })
        });

        return async () => {
            handle()
            const queueMessage = await bufferQueue.read();

            if (isEOF(queueMessage)) {
                return {
                    done: true,
                    value: undefined
                } as IMessage<T>
            }

            const [type, message] = queueMessage as IBufferMessage<IMessage<T>>

            if (type === ERROR) {
                throw message
            }

            return message as IMessage<T>
        }
    })
}
