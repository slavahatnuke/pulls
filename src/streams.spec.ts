import {Pipe, pipeline} from "./index";
import {sequence} from "./sequence";
import {toArray} from "./toArray";
import {tap} from "./tap";
import {delay} from "./lib/delay";
import {streams} from "./streams";

it('streams', async function () {
    const out = await pipeline(
        streams(
            Pipe(
                sequence(3),
                (id) => `A.${id}`,
                tap(async () => delay(10))
            ), // A.0, A.1, A.2
            Pipe(
                sequence(3),
                (id) => `B.${id}`,
                tap(async () => delay(100))
            ), // B.0, B.1, B.2
        ),
        toArray()
    );

    expect(out).toEqual([
        "A.0",
        "A.1",
        "A.2",
        "B.0",
        "B.1",
        "B.2"
    ])
});



it('streams / diff size', async function () {
    const out = await pipeline(
        streams(
            Pipe(
                [],
                (id) => `0.${id}`,
                tap(async () => delay(10))
            ),
            Pipe(
                sequence(3),
                (id) => `A.${id}`,
                tap(async () => delay(10))
            ), // A.0, A.1, A.2
            Pipe(
                sequence(2),
                (id) => `B.${id}`,
                tap(async () => delay(100))
            ), // B.0, B.1
        ),
        toArray()
    );

    expect(out).toEqual([
        "A.0",
        "A.1",
        "A.2",
        "B.0",
        "B.1"
    ])
});


it('streams / errors', async function () {

    await expect(async () => {
        await pipeline(
            streams(
                Pipe(
                    sequence(3),
                    (id) => `A.${id}`,
                    tap(async (idx) => {
                        throw new Error(`A.Error ${idx}`)
                    })
                ), // A.0, A.1, A.2
                Pipe(
                    sequence(3),
                    (id) => `B.${id}`,
                    tap(async (idx) => {
                        throw new Error(`B.Error ${idx}`)
                    })
                ), // B.0, B.1, B.2
            )
        );
    }).rejects.toThrow('A.Error A.0')

    await expect(async () => {
        await pipeline(
            streams(
                Pipe(
                    sequence(3),
                    (id) => `A.${id}`,
                    tap(async (idx) => {

                    })
                ), // A.0, A.1, A.2
                Pipe(
                    sequence(3),
                    (id) => `B.${id}`,
                    tap(async (idx) => {
                        throw new Error(`B.Error ${idx}`)
                    })
                ), // B.0, B.1, B.2
            )
        );
    }).rejects.toThrow('B.Error B.0')
});
