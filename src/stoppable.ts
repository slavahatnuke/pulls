import {IMessage, IStream, Read, Stream, toStream} from "./index";
import {mem} from "./lib/mem";

export type IStoppableInterface = {
    stop: () => Promise<void>,
    isStopped: () => boolean
}

export type IStoppable<T extends any> = IStream<T> & IStoppableInterface

export function stoppable<T extends any>(stream: IStream<T>, onStop: () => Promise<void> | void = async () => undefined): IStoppable<T> {
    const _stream = toStream(stream) as IStoppable<T>;

    let stopped = false;

    const stoppableStream = Stream<T>(() => {
        const read = Read<T>(_stream);

        return async () => {
            if (stopped) {
                return {
                    done: true,
                    value: undefined
                } as IMessage<T>
            } else {
                const message = await read();

                if (stopped) {
                    return {
                        done: true,
                        value: undefined
                    } as IMessage<T>
                }

                return message;
            }
        }
    }) as IStoppable<T>;

    stoppableStream.stop = mem(async () => {
        stopped = true;
        await onStop()
    })

    stoppableStream.isStopped = () => {
        return stopped;
    }

    return stoppableStream
}
