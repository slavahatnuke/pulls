import {tickAsync} from "./lib/tick";
import {tap} from "./tap";

export function tick<T extends any>() {
    return tap<T>(async (x) => await tickAsync(() => undefined))
}
