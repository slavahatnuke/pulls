import {cursorToStream, isCursor} from "./cursor";
import {ICursor, pull} from "./index";

it('cursorToStream', async function () {
    let idx = 0;
    const cursor = {next: () => ({done: idx >= 3, value: idx++})} as ICursor<number>;
    const stream = cursorToStream(cursor);

    expect(await pull(stream)).toEqual(2)
});

it('isCursor', function () {
    const cursor = {next: () => ({done: true, value: undefined})};
    expect(isCursor(cursor)).toEqual(true)
});


it('isCursor = no', function () {
    const cursor = null;
    expect(isCursor(cursor)).toEqual(false)
});
