import {IMapper, IMessage, INode, IToFunctionInput, node, toFunction} from "./index";
import {BufferQueue} from "./lib/buffer";
import {concurrency, IStopConcurrency} from "./lib/concurrency";
import {isEOF} from "./lib/eof";
import {mem} from "./lib/mem";

type IMessageType = 0 | 1;
const TYPE_OK = 0;
const TYPE_ERROR = 1;

export function scale<Input extends any, Output extends any>(quantity: IToFunctionInput<number>, mapper: IMapper<Input, Output>): INode<Input, Output> {
    const desired = toFunction<number>(quantity);

    return node<Input, Output>((read) => {
        const inputs = BufferQueue<IMessage<Input>>(desired);
        const outputs = BufferQueue<[IMessageType, IMessage<Output> | Error]>(desired);

        let stopReading: undefined | IStopConcurrency = undefined
        let stopWriting: undefined | IStopConcurrency = undefined

        let finished = false;

        const done = {
            done: true,
            value: undefined
        } as IMessage<Output>;

        const handle = mem(() => {
            stopReading = concurrency(1, async () => {
                try {
                    const message = await read();
                    await inputs.write(message)

                    if (message.done) {
                        stopReading && stopReading()
                        await inputs.finish()
                    }
                } catch (error) {
                    await outputs.write([TYPE_ERROR, error as Error])
                }
            })


            stopWriting = concurrency(desired, async () => {
                try {
                    const inputMessage = await inputs.read();

                    if (isEOF(inputMessage) || (inputMessage as IMessage<Input>).done) {
                        stopWriting && stopWriting()
                            .then(async () => {
                                finished = true
                                await outputs.write([TYPE_OK, done])
                            })
                        return;
                    }

                    const message = inputMessage as IMessage<Input>

                    try {
                        const resultMessage = {
                            done: false,
                            value: await mapper(message.value)
                        } as IMessage<Output>;

                        await outputs.write([TYPE_OK, resultMessage])
                    } catch (error) {
                        await outputs.write([TYPE_ERROR, error as Error])
                    }
                } catch (error) {
                    await outputs.write([TYPE_ERROR, error as Error])
                }
            })
        })

        return async () => {
            handle()

            if (finished) {
                return done;
            }

            const outputMessage = await outputs.read();

            if (isEOF(outputMessage)) {
                stopReading && stopReading()
                stopWriting && stopWriting()
                inputs.finish()
                outputs.finish()
                return done;
            }

            const [type, payload] = outputMessage as [IMessageType, IMessage<Output>];

            if (type === TYPE_ERROR) {
                throw payload
            } else {
                return payload as IMessage<Output>
            }
        }
    })
}
