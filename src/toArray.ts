import {reduce} from "./reduce";

export function toArray<T extends any>() {
    return reduce<T, T[]>((accumulator, value) => [...accumulator, value], [] as T[])
}
