import {Pipe, pipeline} from "./index";
import {streamsSync} from "./streamsSync";
import {sequence} from "./sequence";
import {toArray} from "./toArray";
import {tap} from "./tap";
import {delay} from "./lib/delay";

it('streamsSync', async function () {

    const out = await pipeline(
        streamsSync(
            sequence(3), // 0, 1, 2
            sequence(3), // 0, 1, 2
            sequence(3), // 0, 1, 2
        ),
        toArray()
    );

    expect(out).toEqual([0, 0, 0, 1, 1, 1, 2, 2, 2])
});


it('streamsSync / diff size', async function () {

    const out = await pipeline(
        streamsSync(
            sequence(3), // 0, 1, 2
            sequence(2), // 0, 1
            sequence(1), // 0
            sequence(0), // nothing
        ),
        toArray()
    );

    expect(out).toEqual([0, 0, 0, 1, 1, 2])
});


it('streamsSync A->B', async function () {

    const out = await pipeline(
        streamsSync(
            Pipe(
                sequence(3),
                (id) => `A.${id}`,
                tap(async () => delay(10))
            ), // A.0, A.1, A.2
            Pipe(
                sequence(3),
                (id) => `B.${id}`,
                tap(async () => delay(100))
            ), // B.0, B.1, B.2
        ),
        toArray()
    );

    expect(out).toEqual([
        "A.0",
        "B.0",
        "A.1",
        "B.1",
        "A.2",
        "B.2"
    ])
});
