import {IMapper, INode, IToFunctionInput, pipe} from "./index";
import {batch} from "./batch";
import {flat} from "./flat";

export function scaleSync<Input extends any, Output extends any>(quantity: IToFunctionInput<number>, mapper: IMapper<Input, Output>): INode<Input, Output> {
    return pipe<Input, Output>(
        batch<Input>(quantity),
        async (values: Input[]) => Promise.all(
            values.map((value: Input) => mapper(value) as Output)),
        flat<Input[], Output>()
    ) as INode<Input, Output>
}
