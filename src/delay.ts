import {IMapper, INode, IToFunctionInput, toFunction} from "./index";
import {tap} from "./tap";
import {delay as libDelay} from "./lib/delay";

export function delay<T extends any>(ms: IToFunctionInput<number>): IMapper<T, T> | INode<T, T> {
    const toMs = toFunction<number>(ms);
    return tap<T>(async () => libDelay(toMs()))
}
