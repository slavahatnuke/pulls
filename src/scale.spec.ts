import {pipeline} from "./index";
import {sequence} from "./sequence";
import {scale} from "./scale";
import {delay} from "./lib/delay";
import {toArray} from "./toArray";
import {tap} from "./tap";

it('scale', async function () {


    const d1 = Date.now()
    const out = await pipeline(
        sequence(4),
        scale<number, number>(1, async (value) => {
            await delay(100)
            return value
        }),
        toArray()
    ) as any[];

    const d2 = Date.now()

    expect(out.sort()).toEqual([0, 1, 2, 3])
    expect(Math.round((d2 - d1) / 100)).toEqual(4)
});


it('scale 2', async function () {

    const d1 = Date.now()
    const out = await pipeline(
        sequence(4),
        scale<number, number>(2, async (value) => {
            await delay(100)
            return value
        }),
        toArray()
    ) as any[];

    const d2 = Date.now()

    expect(out.sort()).toEqual([0, 1, 2, 3])
    expect(Math.round((d2 - d1) / 100)).toEqual(2)
});

it('scale 4', async function () {


    const d1 = Date.now()
    let idx = 0

    const out = await pipeline(
        sequence(4),
        scale<number, number>(4, async (value) => {
            await delay(100)
            idx++
            return value
        }),
        toArray()
    ) as any[];

    const d2 = Date.now()

    expect(out.sort()).toEqual([0, 1, 2, 3])
    expect(idx).toEqual(4)
    expect(Math.round((d2 - d1) / 100)).toEqual(1)
});

it('scale 3', async function () {
    const d1 = Date.now()
    let idx = 0

    const out = await pipeline(
        sequence(4),
        scale<number, number>(3, async (value) => {
            await delay(100)
            idx++
            return value
        }),
        toArray()
    ) as any[];

    const d2 = Date.now()

    expect(out.sort()).toEqual([0, 1, 2, 3])
    expect(idx).toEqual(4)
    expect(Math.round((d2 - d1) / 100)).toEqual(2)
});


it('scale 3 in dynamic', async function () {
    const d1 = Date.now()
    let idx = 0

    const out = await pipeline(
        sequence(4),
        scale<number, number>(3, async (value) => {
            idx++
            await delay(100 * idx); // 100, 200, 300, 400

            return value
        }),
        toArray()
    ) as any[];

    const d2 = Date.now()

    expect(out.sort()).toEqual([0, 1, 2, 3])
    expect(idx).toEqual(4)
    expect(Math.round((d2 - d1) / 100)).toEqual(5)
});

it('scale / error', async function () {
    await expect(async () => {
        await pipeline(
            sequence(4),
            scale<number, number>(4, async (value) => {
                await delay(100)
                throw new Error(`Woooppepeeee`)
            }),
            toArray()
        )
    }).rejects.toThrow(`Woooppepeeee`)
});

it('scale / source / error', async function () {
    await expect(async () => {
        await pipeline(
            sequence(4),
            tap(() => {
                throw new Error('Source Wooopppppeeerrrs')
            }),
            scale<number, number>(4, async (value) => {
                throw new Error(`Unexpected error`)
                return value
            }),
            toArray()
        )
    }).rejects.toThrow(`Source Wooopppppeeerrrs`)
});
