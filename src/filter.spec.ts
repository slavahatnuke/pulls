import {pipeline} from "./index";
import {filter} from "./filter";
import {map} from "./map";
import {tap} from "./tap";

it('filter', async function () {

    const expected: any = [];
    const collect = (value: any) => expected.push(value);

    await pipeline(
        [1, 2, 3],
        // tap(console.log),
        map((x) => x * 2),
        filter((x) => x > 2),
        tap(collect),
        // tap(console.log),
    )

    expect(expected).toEqual([
        4,
        6
    ])
});
