import {pipeline} from "../../index";
import {sequence} from "../../sequence";
import {filter} from "../../filter";
import {tap} from "../../tap";

const {Speed: IndexSpeed} = require('uspeed');
const speed = IndexSpeed('pulls:speed');

function pass(x: any) : any {
    return x
}

async function run() {

    console.log('')
    console.log('>> TAP speed')

    await pipeline(
        sequence(2e6),
        // tap(pass),
        // tap(pass),
        // tap(pass),
        tap(speed)
    )

    console.log('')
    console.log('>> FILTER + TAP speed')


    await pipeline(
        sequence(2e6),
        filter((value) => !(value % 2)),
        tap(speed),
    )
}

run()
    .catch(console.error)
