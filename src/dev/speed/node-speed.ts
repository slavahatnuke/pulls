import {promisify} from "util";
import * as stream from "stream";

const {Readable, Writable, Transform} = stream;
const pipeline = promisify(stream.pipeline);

const {Speed: IndexSpeed} = require('uspeed');
const speed = IndexSpeed('pulls:speed:node');

function sequence(size = 1) {
    let index = 0;
    return new Readable({
        objectMode: true,
        read() {
            index++;

            if (index > size) {
                this.push(null);
            } else {
                this.push(index);
            }
        }
    })
}

function tap(fn = (x: any): any => null) {
    return new Transform({
        objectMode: true,
        async transform(chunk: any, encoding: string, callback: any) {

            try {
                await fn(chunk)
                callback(null, chunk)
            } catch (error) {
                callback(error)
            }
        }
    })
}

function writer(fn = (x: any): any => null) {
    return new Writable({
        objectMode: true,
        async write(chunk: any, encoding: string, callback: any) {
            await fn(chunk)
            callback(null)
        }
    })
}

function pass(x: any): any {
    return x
}

async function run() {

    console.log('')
    console.log('>> TAP node speed')

    await pipeline(
        sequence(2e6),
        // tap(pass),
        // tap(pass),
        // tap(pass),
        writer(speed)
    )
}

run()
    .catch(console.error)
