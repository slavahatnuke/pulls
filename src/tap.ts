import {IMapper, INode, isFunction} from "./index";
import {map} from "./map";

export function tap<Input extends any>(worker: IMapper<Input, any>): IMapper<Input, Input> | INode<Input, Input>  {
    if (!isFunction(worker)) {
        throw new Error(`Expects function`)
    }
    return map<Input, Input>(async (value: Input) => {
        await (worker as IMapper<Input, any>)(value)
        return value
    })
}
