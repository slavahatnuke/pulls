import {IMapper, IMessage, INode, IRead, isFunction, node} from "./index";

export function filter<Input extends any>(condition: IMapper<Input, boolean>): INode<Input, Input> {
    if (!isFunction(condition)) {
        throw new Error(`Expects function`)
    }
    return node<Input, Input>((read): IRead<Input> => async () => {
        let message: IMessage<Input> = {done: true, value: undefined as any};
        let lookingForResult = true

        while (lookingForResult) {
            message = await read();
            const {done, value} = message;
            lookingForResult = !(done || await condition(value));
        }

        return message;
    });
}
