import {IMessage, IStream, stream} from "./index";

export type ICheckEntity = ((x: any) => boolean) | ((x: any) => Promise<boolean>);

export type IReadEntity<EntityType extends any> = () => EntityType | undefined;

export function entities<EntityType extends any>(read: IReadEntity<EntityType>, isEntity: ICheckEntity = (x: EntityType) => !!x): IStream<EntityType> {
    return stream<EntityType>(async () => {
        const entity = await read() as EntityType;

        if (await isEntity(entity)) {
            return {done: false, value: entity} as IMessage<EntityType>
        } else {
            return {done: true, value: undefined as any} as IMessage<EntityType>
        }
    })
}
