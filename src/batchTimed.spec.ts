import {pipeline} from "./index";
import {batchTimed} from "./batchTimed";
import {sequence} from "./sequence";
import {delay} from "./lib/delay";
import {toArray} from "./toArray";
import {tap} from "./tap";
import {catchError} from "./catchError";

it('batchTimed / no timer', async function () {

    const out = await pipeline(
        sequence(3),
        async (id) => {
            await delay(50)
            return id
        },
        batchTimed(2, 0),
        toArray()
    );

    expect(out).toEqual([[0, 1], [2]])
});


it('batchTimed / small timer', async function () {

    const out2 = await pipeline(
        sequence(5),
        async (id) => {
            await delay(50)
            return id
        },
        batchTimed(2, 10),
        toArray()
    );

    expect(out2).toEqual([[0], [1], [2], [3], [4]])
});


it('batchTimed / bigger timer', async function () {

    const out2 = await pipeline(
        sequence(5),
        async (id) => {
            await delay(50)
            return id
        },
        batchTimed(2, 200),
        toArray()
    );

    expect(out2).toEqual([[0, 1], [2], [3, 4]])
});


it('batchTimed / size required', async function () {
    await expect(async () => {
        await pipeline(
            sequence(5),
            async (id) => {
                await delay(50)
                return id
            },
            batchTimed(0, 180),
            toArray()
        );
    }).rejects.toThrow('Size is required')
});

it('batchTimed / batched by time', async function () {

    const out = await pipeline(
        sequence(5),
        async (id) => {
            await delay(50)
            return id
        },
        batchTimed(1e3, 180),
        toArray()
    );

    expect(out).toEqual([[0, 1, 2], [3, 4]])
});


it('batchTimed / batched by size and time', async function () {

    const out = await pipeline(
        sequence(5),
        async (id) => {
            await delay(50)
            return id
        },
        batchTimed(2, 180),
        toArray()
    );

    expect(out).toEqual([[0, 1], [2], [3, 4]])
});


it('batchTimed / error', async function () {

    await expect(async () => {
        await pipeline(
            sequence(5),
            async (id) => {

                throw new Error('woooppeee11')
            },
            batchTimed(2, 180),
            toArray()
        )
    }).rejects.toThrow('woooppeee11')
});


it('batchTimed / source errors catched', async function () {

    expect(await pipeline(
        [1, 2, 3],
        tap(async (id) => {
            throw new Error(`Woooohhh ${id}`)
        }),
        batchTimed(2, 10),
        catchError((error, resolveAs) => resolveAs(error.message)), // snoozed by catchError
        toArray()
    )).toEqual(["Woooohhh 1", "Woooohhh 2", "Woooohhh 3"])
});
