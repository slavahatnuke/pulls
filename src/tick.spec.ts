import {pipeline} from "./index";
import {tick} from "./tick";
import {toArray} from "./toArray";

it('tick', async function () {

    expect(await pipeline(
        [1, 2, 22],
        tick(),
        toArray()
    )).toEqual([1, 2, 22])
});
