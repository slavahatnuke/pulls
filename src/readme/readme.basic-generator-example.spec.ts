import {pipeline} from "../index";
import {tap} from "../tap";

it('Basic generator example', async function () {
    // consume generator/async iterable protocol as stream
    async function* Generate123() {
        yield 1
        yield 2
        yield Promise.resolve(3)
    }

    const expected: number[] = [];
    const collect = (value: number) => expected.push(value);

    await pipeline(
        Generate123(), // consume generator as stream
        tap<number>(collect) // collect output to array as expected
    )

    // expected: [1, 2, 3]
    expect(expected).toEqual([1, 2, 3])
})
