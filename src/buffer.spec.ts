import {pipeline, stream} from "./index";
import {buffer} from "./buffer";
import {toArray} from "./toArray";

it('buffer', async function () {
    const out = await pipeline(
        [1, 2, 3],
        buffer<number>(2),
        toArray()
    );

    expect(out).toEqual([
        1,
        2,
        3
    ])
});


it('buffer / errors', async function () {
    await expect(async () => {
        await pipeline(
            stream(() => {
                throw new Error(`woopss`)
            }),
            buffer<number>(2),
            toArray()
        )
    }).rejects.toThrow(`woopss`)
});
