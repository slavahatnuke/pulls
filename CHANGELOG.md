# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.7.47](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.46...v1.7.47) (2021-02-15)

### [1.7.46](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.45...v1.7.46) (2021-02-15)

### [1.7.45](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.44...v1.7.45) (2021-02-15)

### [1.7.44](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.43...v1.7.44) (2021-02-15)

### [1.7.43](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.42...v1.7.43) (2020-11-30)

### [1.7.42](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.41...v1.7.42) (2020-11-30)

### [1.7.41](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.40...v1.7.41) (2020-11-20)

### [1.7.40](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.39...v1.7.40) (2020-11-20)

### [1.7.39](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.38...v1.7.39) (2020-11-13)

### [1.7.38](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.37...v1.7.38) (2020-11-13)

### [1.7.37](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.36...v1.7.37) (2020-11-02)

### [1.7.36](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.35...v1.7.36) (2020-11-02)

### [1.7.35](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.34...v1.7.35) (2020-11-02)

### [1.7.34](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.33...v1.7.34) (2020-10-28)

### [1.7.33](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.32...v1.7.33) (2020-10-28)

### [1.7.32](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.31...v1.7.32) (2020-10-17)

### [1.7.31](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.30...v1.7.31) (2020-10-16)

### [1.7.30](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.29...v1.7.30) (2020-10-16)

### [1.7.29](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.28...v1.7.29) (2020-10-16)

### [1.7.28](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.27...v1.7.28) (2020-10-16)

### [1.7.27](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.26...v1.7.27) (2020-10-16)

### [1.7.26](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.25...v1.7.26) (2020-10-16)

### [1.7.25](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.24...v1.7.25) (2020-10-16)

### [1.7.24](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.23...v1.7.24) (2020-10-16)

### [1.7.23](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.22...v1.7.23) (2020-10-16)

### [1.7.22](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.21...v1.7.22) (2020-10-16)

### [1.7.21](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.20...v1.7.21) (2020-10-15)

### [1.7.20](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.19...v1.7.20) (2020-10-15)

### [1.7.19](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.18...v1.7.19) (2020-10-15)

### [1.7.18](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.17...v1.7.18) (2020-10-15)

### [1.7.17](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.16...v1.7.17) (2020-09-04)

### [1.7.16](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.15...v1.7.16) (2020-09-04)

### [1.7.15](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.14...v1.7.15) (2020-09-04)

### [1.7.14](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.13...v1.7.14) (2020-09-02)

### [1.7.13](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.12...v1.7.13) (2020-08-19)

### [1.7.12](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.11...v1.7.12) (2020-08-19)

### [1.7.11](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.10...v1.7.11) (2020-08-15)

### [1.7.10](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.9...v1.7.10) (2020-08-15)

### [1.7.9](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.8...v1.7.9) (2020-08-15)

### [1.7.8](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.7...v1.7.8) (2020-08-15)

### [1.7.7](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.6...v1.7.7) (2020-08-15)

### [1.7.6](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.5...v1.7.6) (2020-08-14)

### [1.7.5](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.4...v1.7.5) (2020-08-14)

### [1.7.4](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.3...v1.7.4) (2020-08-12)

### [1.7.3](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.2...v1.7.3) (2020-08-12)

### [1.7.2](https://gitlab.com/slavahatnuke/pulls/compare/v1.7.1...v1.7.2) (2020-05-22)

### [1.7.1](https://gitlab.com/slavahatnuke/pulls/compare/v1.6.5...v1.7.1) (2020-05-22)

### [1.6.5](https://gitlab.com/slavahatnuke/pulls/compare/v1.6.4...v1.6.5) (2020-05-22)

### [1.6.4](https://gitlab.com/slavahatnuke/pulls/compare/v1.6.3...v1.6.4) (2020-05-22)

### [1.6.3](https://gitlab.com/slavahatnuke/pulls/compare/v1.6.2...v1.6.3) (2020-05-22)

### 1.6.2 (2020-05-22)
