- Add operator to process different cases; naming: cases, nodes, branch

- Pause / Unpause functionality ??

- Improve Node.js writable integration
    - https://nodejs.org/api/stream.html#stream_writable_streams

- Slice to streams its similar to batch or batchTimed but it turns data to much bigger streams like 1M or 100M batches. As result stream should be sliced to smaller ones.
